#ifndef LEVEL3_H
#define LEVEL3_H

#include "../Aux/Aux.h"
#include "../Aux/Objet.h"
#include "../Aux/Personnage.h"

class Niveau3 {
private:
    int tailleX = 9;
    int tailleY = 3;
    char entree;
    std::vector<Objet> jeuMatrix;
    std::vector<Objet> decorGauche;
    std::vector<Objet> decorDroite;
    std::vector<Objet> obstacles;
    Personnage moto;
public:
    Niveau3(Personnage _moto); // Constructor declaration
    void creerDecorGauche();
    void creerDecorDroite();
    void creerObstacles();
    void defilement();
    void afficher();
    bool collision();
    void miseAJour();
    std::vector<Objet> getDecorDroite() const { return decorDroite; }
    std::vector<Objet> getDecorGauche() const { return decorGauche; }
    std::vector<Objet> getObstacles() const { return obstacles; }
    Personnage getmoto() const { return moto; }
    int getTailleX() const { return tailleX; }
    int getTailleY() const { return tailleY; }
};

#endif
