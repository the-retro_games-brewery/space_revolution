#ifndef SFML_level3_H
#define SFML_level3_H

#include "level3.h"

class SFML_level3 {
private:
    Niveau3 niveau;
    sf::RenderWindow& window;
    sf::Texture motoTexture;
    sf::Texture decorGaucheTexture;
    sf::Texture decorDroiteTexture;
    sf::Texture obstacleTexture;
    sf::Texture Background;
    float roadOffset = 0;
    float groundOffset = 0;
    sf::Clock gameClock; // Rename the clock to avoid confusion

public:
    SFML_level3(sf::RenderWindow& window, int largeur, int hauteur);
    void TextureLoader();
    void Render(int largeur, int hauteur);
    void run(int largeur, int hauteur);
};
#endif //SFML_level3_H