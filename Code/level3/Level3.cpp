#include "level3.h"

Niveau3::Niveau3(Personnage _moto) : moto(_moto) {
}

void Niveau3::creerDecorGauche() {
    decorGauche.push_back(Objet(0, 0, 1));
}

void Niveau3::creerDecorDroite() {
    decorDroite.push_back(Objet(8, 0, 1));
}

void Niveau3::creerObstacles() {
    rand();
    if (rand() % 8 - 1 == 0) {
        obstacles.push_back(Objet(1, 0, 1));
    }
    if (rand() % 8 - 1 == 0) {
        obstacles.push_back(Objet(2, 0, 1));
    }
    if (rand() % 8 - 1 == 0) {
        obstacles.push_back(Objet(3, 0, 1));
    }
    if (rand() % 8 - 1 == 0) {
        obstacles.push_back(Objet(4, 0, 1));
    }
    if (rand() % 8 - 1 == 0) {
        obstacles.push_back(Objet(5, 0, 1));
    }
    if (rand() % 8 - 1 == 0) {
        obstacles.push_back(Objet(6, 0, 1));
    }
    if (rand() % 8 - 1 == 0) {
        obstacles.push_back(Objet(7, 0, 1));
    }
}


bool Niveau3::collision() {
    for (auto &obstacle : obstacles) {
        if (obstacle.getX() == moto.getX() && obstacle.getY() == moto.getY()) {
            return true;
        }
        else{
            return false;
        }
    }
}

void Niveau3::miseAJour() {
    // Défilement de la grille de jeu

       creerObstacles();
       creerDecorGauche();
       creerDecorDroite();

       for(auto& obstacle : obstacles){
           obstacle.setY(obstacle.getY() + 1);
       }

         for(auto& decor : decorGauche){
              decor.setY(decor.getY() + 1);
         }

            for(auto& decor : decorDroite){
                decor.setY(decor.getY() + 1);
            }
    
}

void Niveau3::afficher() {
    // Efface l'écran
    system("clear");

    // Affiche la grille de jeu
    for (int y = 0; y < tailleY; ++y) {
        for (int x = 0; x < tailleX; ++x) {
            // Vérifie si la moto est à cette position
            if (x == moto.getX() && y == moto.getY()) {
                std::cout << "M";
            }
                // Vérifie si un obstacle est à cette position
            else {
                bool obstacleFound = false;
                for (auto &obstacle : obstacles) {
                    if (x == obstacle.getX() && y == obstacle.getY()) {
                        obstacleFound = true;
                        std::cout << "O";
                        break;
                    }
                }
                // Vérifie si un décor de gauche est à cette position
                if (!obstacleFound) {
                    for (auto &decor : decorGauche) {
                        if (x == decor.getX() && y == decor.getY()) {
                            std::cout << "|";
                            obstacleFound = true;
                            break;
                        }
                    }
                }
                // Vérifie si un décor de droite est à cette position
                if (!obstacleFound) {
                    for (auto &decor : decorDroite) {
                        if (x == decor.getX() && y == decor.getY()) {
                            std::cout << "|";
                            obstacleFound = true;
                            break;
                        }
                    }
                }
                // Si aucun objet n'est à cette position, affiche un espace vide
                if (!obstacleFound) {
                    std::cout << " ";
                }
            }
        }
        std::cout << std::endl;
    }
}
/*
int main() {
    Personnage moto(5, 3, 3, 1);
    Niveau3 niveau(moto);
    niveau.miseAJour();
    return 0;
}
*/
// ligne de commande pour compiler le programme avec c++17
// g++ -std=c++17 -o level3 Level3.cpp ../Aux/Personnage.cpp ../Aux/Objet.cpp