#include "SFML_level3.h"
constexpr float ROUTE_HEIGHT_PERCENTAGE = 0.6f;

SFML_level3::SFML_level3(sf::RenderWindow& window, int largeur, int hauteur) : window(window), niveau(Personnage(4,3,3,1)) {
}

void SFML_level3::TextureLoader() {
    motoTexture.loadFromFile("../assets/Backgrounds/motorbike/player.png");
    decorGaucheTexture.loadFromFile("../assets/Backgrounds/motorbike/decor5.png");
    decorDroiteTexture.loadFromFile("../assets/Backgrounds/motorbike/decor3.png");
    obstacleTexture.loadFromFile("../assets/Backgrounds/motorbike/decor2.png");
    Background.loadFromFile("../assets/Backgrounds/motorbike/background.jpg");
}

void SFML_level3::Render(int largeur, int hauteur) {
float routeHeight = hauteur * ROUTE_HEIGHT_PERCENTAGE;

    window.clear();
    sf::Sprite backgroundSprite(Background);
    backgroundSprite.setScale(static_cast<float>(largeur) / Background.getSize().x, static_cast<float>(hauteur) / Background.getSize().y);
    window.draw(backgroundSprite);

const int nbRectangles = 5;
sf::VertexArray ground(sf::Quads, 4 * nbRectangles);
float elapsedTime = gameClock.getElapsedTime().asSeconds();
groundOffset += elapsedTime * 0.8f; // Vitesse de déplacement du sol
gameClock.restart();

float horizon = hauteur * 0.6f; // Ligne d'horizon à 60% de la hauteur de la fenêtre
float rectangleHeight = horizon / nbRectangles; // Hauteur ajustée pour chaque rectangle

for (int i = 0; i < nbRectangles; ++i) {
    float yPosition = hauteur - i * rectangleHeight;
    float rectangleWidth = largeur * (1 - float(i) / nbRectangles) + 100000; // Largeur du rectangle

    ground[i * 4].position = sf::Vector2f(largeur / 2 - rectangleWidth / 2, yPosition + rectangleHeight);
    ground[i * 4 + 1].position = sf::Vector2f(largeur / 2 + rectangleWidth / 2, yPosition + rectangleHeight);
    ground[i * 4 + 2].position = sf::Vector2f(largeur / 2 + rectangleWidth / 2, yPosition);
    ground[i * 4 + 3].position = sf::Vector2f(largeur / 2 - rectangleWidth / 2, yPosition);

    float colorFactor = std::fabs(std::sin(groundOffset + i * 0.5f));
sf::Color couleur = sf::Color(100 * colorFactor, 50 * colorFactor, 50 * (1 - colorFactor));
    for (int j = 0; j < 4; ++j) {
        ground[i * 4 + j].color = couleur;
    }
}

window.draw(ground);


    
    const int nbTrapezes = 5;
    sf::VertexArray road(sf::Quads, 4 * nbTrapezes);
    roadOffset += elapsedTime * 0.8f; // Vitesse de déplacement de la route
    gameClock.restart();

    float trapezeHeight = horizon / nbTrapezes; // Hauteur ajustée pour chaque trapèze

    for (int i = 0; i < nbTrapezes; ++i) {
        float yPosition = hauteur - i * trapezeHeight;
        float trapezeWidthTop = largeur * (1 - float(i) / nbTrapezes)+100;
        float trapezeWidthBottom = largeur * (1 - float(i + 1) / nbTrapezes)+100;


        road[i * 4].position = sf::Vector2f(largeur / 2 - trapezeWidthTop / 2, yPosition+ trapezeHeight);
        road[i * 4 + 1].position = sf::Vector2f(largeur / 2 + trapezeWidthTop / 2, yPosition+ trapezeHeight);
        road[i * 4 + 2].position = sf::Vector2f(largeur / 2 + trapezeWidthBottom / 2, yPosition );
        road[i * 4 + 3].position = sf::Vector2f(largeur / 2 - trapezeWidthBottom / 2, yPosition );

        float colorFactor = std::fabs(std::sin(roadOffset + i * 0.5f));
        sf::Color couleur = sf::Color(255 * colorFactor, 100 * colorFactor, 100 * (1 - colorFactor));
        for (int j = 0; j < 4; ++j) {
            road[i * 4 + j].color = couleur;
        }
    }

    window.draw(road);

    


    // Dessiner la moto, les décors et les obstacles
    sf::Sprite motoSprite(motoTexture);
    motoSprite.setPosition(niveau.getmoto().getX() * tailleTuileX, routeHeight);
    window.draw(motoSprite);

    for (auto& decor : niveau.getDecorGauche()) {
        sf::Sprite decorSprite(decorGaucheTexture);
        decorSprite.scale(0.5, 0.5);
        decorSprite.setPosition(decor.getX() * tailleTuileX, routeHeight);
        window.draw(decorSprite);
    }

    for (auto& decor : niveau.getDecorDroite()) {
        sf::Sprite decorSprite(decorDroiteTexture);
        decorSprite.scale(0.5, 0.5);
        decorSprite.setPosition(decor.getX() * tailleTuileX, routeHeight);
        window.draw(decorSprite);
    }

    for (auto& obstacle : niveau.getObstacles()) {
        sf::Sprite obstacleSprite(obstacleTexture);
        obstacleSprite.scale(0.5, 0.5);
        obstacleSprite.setPosition(obstacle.getX() * tailleTuileX, routeHeight);
        window.draw(obstacleSprite);
    }
    window.display();
}



void SFML_level3::run(int largeur, int hauteur) {
    sf::Clock clock; // Créez une horloge pour mesurer le temps écoulé
    float tempsEntreMisesAJour = 0.3f; // Temps entre les mises à jour en secondes

    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
            } else if (event.type == sf::Event::Resized) {
                // Mettre à jour la taille de la matrice en fonction de la nouvelle taille de la fenêtre
                largeur = event.size.width;
                hauteur = event.size.height;
            }
        }

        // Vérifiez les touches Q et D à chaque itération de la boucle
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q)) {
            niveau.getmoto().setX(niveau.getmoto().getX() - 1);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
            niveau.getmoto().setX(niveau.getmoto().getX() + 1);
        }

        if (gameClock.getElapsedTime().asSeconds() >= 0.3f) {
            Render(largeur, hauteur);
            niveau.miseAJour();
            gameClock.restart();
        }
    }
    window.close();
}




int main() {
    sf::RenderWindow window(sf::VideoMode(1800, 1200), "SFML works!");
    SFML_level3 game(window, 1800, 1200);
    game.TextureLoader();
    game.run(1800, 1200);
    return 0;
}

//g++ -std=c++17 SFML_level3.cpp level3.cpp ../Aux/Personnage.cpp ../Aux/Objet.cpp ../Aux/Coordonnees.cpp ../Aux/Bullet.cpp ../Functions/pause.cpp -o testGLV3 -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio