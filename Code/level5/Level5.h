#ifndef HACKING_FUNCTIONS_H
#define HACKING_FUNCTIONS_H

#include <iostream>
#include <string>
#include <vector>
#include <chrono>
#include <thread>

// Fonction pour découper une chaîne en vecteur de mots
std::vector<std::string> split(const std::string &s, char delim);

// Fonction pour simuler le "hacking"
void hack();

// Fonction principale pour la simulation de piratage
int hacking();

#endif //HACKING_FUNCTIONS_H
