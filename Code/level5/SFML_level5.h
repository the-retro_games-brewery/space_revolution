SFML_level5.h
718 B
 #ifndef SFML_LEVEL5_H
#define SFML_LEVEL5_H
#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <string>
#include "../Aux/aux.h"
class SFML_level5 {
private:
    sf::RenderWindow& window; // Reference to the SFML window
    sf::Texture Background;
    sf::Texture Computer;
    sf::Font Font;
    std::string currentCommand;
    std::vector<std::string> commandHistory;
    void handleInput(sf::Event& event); // Gestion des entrées utilisateur
public:
    // Constructor with a reference to the SFML window
    SFML_level5(sf::RenderWindow& renderWindow);
    void TextureLoader();
    void render(); // Render the game
    void run(int largeur, int hauteur);
};
#endif // SFML_LEVEL5_H