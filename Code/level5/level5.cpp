#include "Level5.h"

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> result;
    std::string word;
    for (char ch : s) {
        if (ch == delim) {
            if (!word.empty()) {
                result.push_back(word);
                word.clear();
            }
        } else {
            word += ch;
        }
    }
    if (!word.empty()) {
        result.push_back(word);
    }
    return result;
}

void hack() {
    std::cout << "----------------------------------------------︎\n";
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "Initializing hacking sequence...\n";
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "Hacking in progress...\n";
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "Accessing mainCore...\n";
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "Bypassing security protocols...\n";
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "Injecting malware...\n";
    std::cout << "Progress: [";
    const int totalProgress1 = 20;
    for (int i = 0; i <= totalProgress1; ++i) {
        std::cout << (i < totalProgress1 ? "⚡" : "💀");
        std::cout.flush();
        std::this_thread::sleep_for(std::chrono::milliseconds(150));
    }
    std::cout << "]\n";
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "Uploading virus...\n";
    std::cout << "Progress: [";
    const int totalProgress2 = 20;
    for (int i = 0; i <= totalProgress2; ++i) {
        std::cout << (i < totalProgress2 ? "⚡" : "️💀");
        std::cout.flush();
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    std::cout << "]\n";
    std::this_thread::sleep_for(std::chrono::seconds(2));
    std::cout << "Access granted! Proceeding with the hack...\n";
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "Hack successful! System compromised.\n";
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "----------------------------------------------︎\n";
}

int Niveau5() {
    std::string currentDirectory = "/home/user";
    std::vector<std::string> files = {"DOOM.exe", "JEFF.data","CompilationDeMusiqueCool.mp3","RienASignaler.txt","System.bat"};
    std::string input;

    std::cout <<"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n"
                "㉿ Bienvenue sur le terminal de contrôle et de gestion de KiloBit Solutions n°4200\n"
                "㉿ Ce terminal est destiné à être utilisé par des professionnels.\n"
                "㉿ Il vous permet de gérer les systèmes informatiques de NanoCorp Solutions de manière sécurisée.\n"
                "㉿ Veuillez noter que l'accès non autorisé à ce terminal est strictement interdit.\n"
                "㉿ Toute tentative de piratage ou d'exploitation de failles de sécurité sera signalée aux autorités compétentes.\n"
                "㉿ Veuillez vous assurer que vous avez l'autorisation appropriée avant d'utiliser ce terminal.\n"
                "㉿ En utilisant ce terminal, vous acceptez de respecter les lois et réglementations en vigueur.\n"
                "㉿ Veuillez noter aussi que ce terminal est surveillé en permanence par notre équipe de sécurité informatique.\n"
                "㉿ Toute utilisation abusive de ce terminal peut entraîner des poursuites judiciaires qui peuvent etrainer la mort.\n"
                "㉿ Attention: l'utilisation de ce terminal peut entraîner des conséquences imprévues.\n"
                "㉿ Si vous rencortez un BUG signalez le a JEFF, si vous etes JEFF LISEZ LE FOUTU MANUEL.\n"

                "㉿ KiloBit Solutions nie toute responsabilité si votre expérience se transforme en cauchemar informatique.\n"
                "㉿ Nous vous recommandons donc d'utiliser ce terminal de façon prudente, voire même avec une certaine paranoïa.\n "
                "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX \n\n";
    while (true) {
        std::cout << "㉿ KBs-Term_N°4200 [" << currentDirectory << "] >> ";
        std::getline(std::cin, input);

        std::vector<std::string> tokens = split(input, ' ');

        if (tokens[0] == "ls") {
            std::cout << "Listing files:\n";
            for (const std::string &file : files) {
                std::cout << file << std::endl;
            }
        } else if (tokens[0] == "cd") {
            if (tokens.size() > 1) {
                currentDirectory = tokens[1];
            } else {
                std::cerr << "Usage: cd [directory]\n";
            }
        } else if (tokens[0] == "SSHConnect") {
            if (tokens.size() > 1) {
                std::cout << "Connecting to " << tokens[1] << "...\n";
            } else {
                std::cerr << "Usage: SSHConnect [Address]\n";
            }
        } else if (tokens[0] == "SSHRequest") {
            if (tokens.size() >= 4 && tokens[2] == "-local") {
                std::string source = tokens[1];
                std::string destination = tokens[3];
                std::cout << "Sending Request ...\n";
                std::this_thread::sleep_for(std::chrono::seconds(1));
                std::cout << "Request recieved by : "<< source <<"\n";
                std::cout << "Downloading file from " << source << " to " << destination << "...\n";
                std::this_thread::sleep_for(std::chrono::seconds(2));
                files.push_back(destination);
            } else {
                std::cerr << "Usage: curl [url] -local [filename]\n";
            }
        } else if (tokens[0] == "chmod") {
            if (tokens.size() > 1) {
                std::cout << "Changing permissions of file " << tokens[1] << "...\n";
            } else {
                std::cerr << "Usage: chmod [options] [file]\n";
            }
        } else if (tokens[0] == "./DOOM.exe") {
            std::cout << "Executing DOOM...\n";
        } else if (tokens[0] == "./Hack.exe") {
            std::cout << "Executing Hack.exe...\n";
            hack();
        } else {
            std::cerr << "Command not found: " << tokens[0] << std::endl;
        }
    }

    return 0;
}