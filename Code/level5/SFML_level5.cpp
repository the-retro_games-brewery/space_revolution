#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <vector>
#include <string>
#include <thread>
#include <chrono>

class SFML_level5 {
private:
    sf::RenderWindow& window;
    sf::Texture Background, Computer, doom;
    sf::Font Font;
    std::vector<std::string> commandHistory;
    std::string currentCommand;
    std::vector<std::string> files = {"DOOM.exe", "JEFF.data", "CompilationDeMusiqueCool.mp3", "RienASignaler.txt", "System.bat"};
    std::string currentDirectory = "/home/user";
    bool hackingInProgress = false;
    bool connected = false;
    bool downloaded = false;
    std::string hackingProgress = "";
    int commandHistoryIndex = 0; // Index pour parcourir l'historique des commandes


    void TextureLoader() {
        if (!Background.loadFromFile("/Users/mac/cours/space_revolution/assets/Backgrounds/Game/Hacking/servrBackground.jpg")) {
            std::cout << "Problème lors du chargement de la texture du fond" << std::endl;
        }
        if (!Computer.loadFromFile("/Users/mac/cours/space_revolution/assets/Backgrounds/Game/Hacking/computer.png")) {
            std::cout << "Problème lors du chargement de la texture de l'ordinateur" << std::endl;
        }
        if (!Font.loadFromFile("/Users/mac/cours/space_revolution/assets/GUI/fonts/computer-pixel-7/computer_pixel-7.ttf")) {
            std::cout << "Problème lors du chargement du font" << std::endl;
        }
        if(!doom.loadFromFile("/Users/mac/cours/space_revolution/assets/Backgrounds/Game/Hacking/easter/doom.jpg")){
            std::cout << "Problème lors du chargement de la texture de doom" << std::endl;
        }
    }
    
      sf::Music music;

    void delay(int mili) {
        std::this_thread::sleep_for(std::chrono::milliseconds(mili));
    }


    void handleInput(sf::Event& event) {
        if (event.type == sf::Event::TextEntered) {
            if (event.text.unicode == '\b' && !currentCommand.empty()) { // Backspace
                currentCommand.pop_back();
            } else if (event.text.unicode == '\r' || event.text.unicode == '\n') { // Enter
                executeCommand(currentCommand);
                commandHistory.push_back(currentCommand);
                currentCommand.clear();
            } else if (event.text.unicode <   158) { // Caractères ASCII standard
                currentCommand += static_cast<char>(event.text.unicode);
            }
        }
    }

    void executeCommand(const std::string& command) {
        std::vector<std::string> tokens = split(command, ' ');
        if (tokens.empty()) return; // Ne rien faire si la commande est vide

        if (tokens[0] == "ls") {
            commandHistory.push_back("Listing files:");
            for (const std::string &file : files) {
                commandHistory.push_back(file);
            }
        }
        else if (tokens[0] == "XFM" && tokens [1] == "-connect") {
            commandHistory.push_back("Contacting XFM server.");
              delay(500);
            commandHistory.push_back("Establishing connection...");
                delay(500);
            commandHistory.push_back("Connection established.");
            connected = true;
        }
        else if (tokens[0] == "XFM" && tokens[1] == "-download" && tokens [2] == "hack.exe" && connected){
            commandHistory.push_back("Downloading hack.exe...");
            commandHistory.push_back("Download complete!");
            downloaded = true;
        }
        else if (tokens[0] == "./hack.exe" && downloaded) {
            commandHistory.push_back("Hacking defenses...");
            hackingInProgress = true;
            for (int i = 0; i <= 100; i += 10) {
                hackingProgress = "Progress: " + std::to_string(i) + "%";
                render();
                delay(200);
            }
            hackingInProgress = false;
            commandHistory.push_back("Defenses hacked successfully!");
        }
        else if (tokens[0] == "./DOOM.exe"){
            sf::Sprite doomSprite;
            doomSprite.setTexture(doom);
            doomSprite.setScale(2,2);
            doomSprite.move(610,350);
            window.draw(doomSprite);
            window.display();
            music.openFromFile("/Users/mac/cours/space_revolution/assets/Backgrounds/Game/Hacking/easter/doommusic.mp3");
            music.play();
            std::this_thread::sleep_for(std::chrono::seconds(15));
            window.clear(); // Clear the window to remove the previous frame
            music.stop();
        }
        else if (tokens[0] == "cat" && tokens[1]==" JEFF.data"){
            commandHistory.push_back("File content: JEFF.data");
            commandHistory.push_back("JEFF est un bon gars.");}
        else if (tokens[0] == "cat" && tokens[1]==" CompilationDeMusiqueCool.mp3"){
            commandHistory.push_back("File content: CompilationDeMusiqueCool.mp3");
            commandHistory.push_back("Impossible de lire un fichier binaire.");}
        else if (tokens[0] == "cat" && tokens[1]==" RienASignaler.txt"){
            commandHistory.push_back("File content: RienASignaler.txt");
            commandHistory.push_back("Rien à signaler.");}
        else if (tokens[0] == "cat" && tokens[1] ==" System.bat"){
            commandHistory.push_back("File content: System.bat");
            commandHistory.push_back("echo 'Hello, World!'");
        }
        else if (tokens[0] == "echo") {
            std::string message = command.substr(5); // Extract the message after "echo "
            commandHistory.push_back("Echo: " + message);
        }
        else if (tokens[0] == "cd" && tokens.size() > 1) {
            std::string directory = tokens[1];
            if (directory == "..") {
                // Go up one directory
                size_t pos = currentDirectory.find_last_of('/');
                if (pos != std::string::npos) {
                    currentDirectory = currentDirectory.substr(0, pos);
                }
            } else {
                // Go to a specific directory
                currentDirectory += "/" + directory;
            }
            commandHistory.push_back("Current directory: " + currentDirectory);
        }
        else if (tokens[0] == "pwd") {
            commandHistory.push_back("Current directory: " + currentDirectory);
        }
        else if (tokens[0] == "mkdir" && tokens.size() > 1) {
            std::string directory = tokens[1];
            std::string newDirectory = currentDirectory + "/" + directory;
            commandHistory.push_back("Creating directory: " + newDirectory);
            // Code to create the directory
        }
        else if (tokens[0] == "rm" && tokens.size() > 1) {
            std::string file = tokens[1];
            std::string filePath = currentDirectory + "/" + file;
            commandHistory.push_back("Deleting file: " + filePath);
            // Code to delete the file
        }
        else if (tokens[0] == "mv" && tokens.size() > 2) {
            std::string source = tokens[1];
            std::string destination = tokens[2];
            std::string sourcePath = currentDirectory + "/" + source;
            std::string destinationPath = currentDirectory + "/" + destination;
            commandHistory.push_back("Moving file: " + sourcePath + " to " + destinationPath);
            // Code to move the file
        }
        else if (tokens[0] == "cp" && tokens.size() > 2) {
            std::string source = tokens[1];
            std::string destination = tokens[2];
            std::string sourcePath = currentDirectory + "/" + source;
            std::string destinationPath = currentDirectory + "/" + destination;
            commandHistory.push_back("Copying file: " + sourcePath + " to " + destinationPath);
            // Code to copy the file
        }
        else if (tokens[0] == "help"){
            commandHistory.push_back("Bienvenue sur le terminal de contrôle et \nde gestion de KiloBit Solutions n°4200");
            commandHistory.push_back("Ce terminal est destiné à être utilisé par des professionnels.");
            commandHistory.push_back("Il vous permet de gérer les systèmes informatiques de \nNanoCorp Solutions de manière sécurisée.");
            commandHistory.push_back("Veuillez noter que l'accès non autorisé à ce \nterminal est strictement interdit.");
            commandHistory.push_back("Toute tentative de piratage ou d'exploitation de failles \nde sécurité sera signalée aux autorités compétentes.");
           commandHistory.push_back("Toute utilisation abusive de ce terminal \npeut entraîner des poursuites judiciaires qui peuvent etrainer la mort.");
             commandHistory.push_back("Attention: l'utilisation de ce terminal peut entraîner des conséquences imprévues.");
             commandHistory.push_back("Si vous rencortez un BUG signalez le a JEFF, \nsi vous etes JEFF LISEZ LE FOUTU MANUEL.");
              commandHistory.push_back("KiloBit Solutions nie toute responsabilité \nsi votre expérience se transforme en cauchemar informatique.");

            

        }

        else {
            commandHistory.push_back("Command not found: " + tokens[0] + "for help use help");
        }

        // Limiter l'historique des commandes à   15 lignes
        while (commandHistory.size() >   15) {
            commandHistory.erase(commandHistory.begin());
        }
    }

    std::vector<std::string> split(const std::string &s, char delim) {
        std::vector<std::string> result;
        std::string word;
        for (char ch : s) {
            if (ch == delim) {
                if (!word.empty()) {
                    result.push_back(word);
                    word.clear();
                }
            } else {
                word += ch;
            }
        }
        if (!word.empty()) {
            result.push_back(word);
        }
        return result;
    }

    void render() {
        sf::Sprite backgroundSprite(Background);
        backgroundSprite.setScale(static_cast<float>(window.getSize().x) / Background.getSize().x, static_cast<float>(window.getSize().y) / Background.getSize().y);
        sf::Sprite computerSprite(Computer);
        computerSprite.setPosition(20, 100);
        computerSprite.setScale(3, 3);

        sf::Text outputText("", Font);
        outputText.setCharacterSize(20);
        outputText.setFillColor(sf::Color::White);
        outputText.setPosition(620, 360);

        std::string history;
        int linesToShow = std::min(commandHistoryIndex +   15, static_cast<int>(commandHistory.size()));
        for (int i = commandHistoryIndex; i < linesToShow; ++i) {
            history += commandHistory[i] + "\n";
        }

        outputText.setString(history + "KBs-Term_N°4200 :> " + currentCommand + "_");

        window.clear();
        window.draw(backgroundSprite);
        window.draw(computerSprite);
        window.draw(outputText);

        // Afficher le progrès du piratage s'il est en cours
        if (hackingInProgress) {
            sf::Text hackingProgressText(hackingProgress, Font);
            hackingProgressText.setCharacterSize(20);
            hackingProgressText.setFillColor(sf::Color::Red);
            hackingProgressText.setPosition(520, 400);
            window.draw(hackingProgressText);
        }

        window.display();
    }

public:
    SFML_level5(sf::RenderWindow& renderWindow) : window(renderWindow) {}

    void run(int largeur, int hauteur) {
        TextureLoader();
        while (window.isOpen()) {
            sf::Event event;
            while (window.pollEvent(event)) {
                if (event.type == sf::Event::Closed) {
                    window.close();
                }
                handleInput(event);
            }
            render();
        }
    }
};

int main() {
    sf::RenderWindow window(sf::VideoMode(1800,   1500), "Level 5");
    SFML_level5 level5(window);
    level5.run(1800,   1500);
    return 0;
}


// ligne pour compiler
// g++ -std=c++11 SFML_level5.cpp -o testGlv5 -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio
