#include "Fonctions.h"

int gameOver(sf::RenderWindow& window) {

    sf::Texture backgroundTexture;
    sf::Texture playButtonTexture;
    sf::Texture playButtonTextureClicked;
    sf::Texture titleTexture;
    sf::Texture quitButtonTexture;
    sf::Texture quitButtonTextureClicked;

    sf::Music music;

    if (!backgroundTexture.loadFromFile("../assets/GUI/gameoverImage.jpg")) {
        std::cout << "Problème lors du chargement de la texture du fond" << std::endl;
    }
    if (!playButtonTexture.loadFromFile("../assets/GUI/retryButton.png")) {
        std::cout << "Problème lors du chargement de la texture du bouton de démarrage" << std::endl;
    }
    if (!playButtonTextureClicked.loadFromFile("../assets/GUI/retryButtonClicked.png")) {
        std::cout << "Problème lors du chargement de la texture du bouton de démarrage" << std::endl;
    }
    if (!titleTexture.loadFromFile("../assets/GUI/GAME-OVER.png")) {
        std::cout << "Problème lors du chargement de la texture du titre" << std::endl;
    }
    if (!quitButtonTexture.loadFromFile("../Assets/GUI/quitButton.png")) {
        std::cout << "Problème lors du chargement de la texture du bouton de musique" << std::endl;
    }
    if (!quitButtonTextureClicked.loadFromFile("../Assets/GUI/quitButtonClicked.png")) {
        std::cout << "Problème lors du chargement de la texture du bouton de musique" << std::endl;
    }
    music.openFromFile("../Sounds/inGame/Another One Bites the Dust.mp3");

    sf::Sprite backgroundSprite(backgroundTexture);
    sf::Sprite playButtonSprite(playButtonTexture);
    sf::Sprite titleSprite(titleTexture);
    sf::Sprite quitButtonSprite(quitButtonTexture);

    float ButtonScaleFactor = 0.5f;
    playButtonSprite.setScale(ButtonScaleFactor, ButtonScaleFactor);
    quitButtonSprite.setScale(ButtonScaleFactor, ButtonScaleFactor);

    backgroundSprite.setScale(
            static_cast<float>(window.getSize().x) / backgroundTexture.getSize().x,
            static_cast<float>(window.getSize().y) / backgroundTexture.getSize().y
    );

    quitButtonSprite.setPosition(530, 400);
    playButtonSprite.setPosition(530, 200);
    titleSprite.setPosition(400, 50);

    music.play();
    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
                return 0;
            } else if (event.type == sf::Event::MouseButtonPressed) {
                sf::Vector2i mousePosition = sf::Mouse::getPosition(window);
                sf::FloatRect playButtonBounds = playButtonSprite.getGlobalBounds();
                sf::FloatRect quitButtonBounds = quitButtonSprite.getGlobalBounds();
                if (playButtonBounds.contains(mousePosition.x, mousePosition.y)) {
                    playButtonSprite.setTexture(playButtonTextureClicked);
                    return 1;
                }
                if (quitButtonBounds.contains(mousePosition.x, mousePosition.y)) {
                    quitButtonSprite.setTexture(quitButtonTextureClicked);
                    return 2;
                }
            } else if (event.type == sf::Event::MouseMoved) {
                sf::Vector2i mousePosition = sf::Mouse::getPosition(window);
                sf::FloatRect playButtonBounds = playButtonSprite.getGlobalBounds();
                sf::FloatRect quitButtonBounds = quitButtonSprite.getGlobalBounds();

                if (playButtonBounds.contains(mousePosition.x, mousePosition.y)) {
                    playButtonSprite.setTexture(playButtonTextureClicked);
                } else {
                    playButtonSprite.setTexture(playButtonTexture);
                }
                if (quitButtonBounds.contains(mousePosition.x, mousePosition.y)) {
                    quitButtonSprite.setTexture(quitButtonTextureClicked);
                } else {
                    quitButtonSprite.setTexture(quitButtonTexture);
                }
            } else if (event.type == sf::Event::KeyPressed) {
                if (event.key.code == sf::Keyboard::Z) {
                    std::cout << "Z key pressed: Move up\n";
                } else if (event.key.code == sf::Keyboard::S) {
                    std::cout << "S key pressed: Move down\n";
                }
            }
        }

        window.clear();

        backgroundSprite.setScale(
            static_cast<float>(window.getSize().x) / backgroundTexture.getSize().x,
            static_cast<float>(window.getSize().y) / backgroundTexture.getSize().y
        );
        window.draw(backgroundSprite);
        window.draw(playButtonSprite);
        window.draw(titleSprite);
        window.draw(quitButtonSprite);

        window.display();
    }
    music.stop();
    return 0;
}
