#include "Fonctions.h"

/*
void loadingScreen(sf::RenderWindow& window) {
    // Load the background image
    sf::Texture backgroundTexture;
    if (!backgroundTexture.loadFromFile("/Users/mac/cours/dev_app/assets/Backgrounds/Scrolling/OIG2.jpg")) {
        // Handle error if the image fails to load
        return;
    }
    sf::Sprite background(backgroundTexture);

    // Load font for text
    sf::Font font;
    if (!font.loadFromFile("/Users/mac/cours/dev_app/assets/GUI/fonts/title.otf")) {
        // Handle error if the font fails to load
        return;
    }

    // Create loading text
    sf::Text loadingText("Loading...", font);
    loadingText.setCharacterSize(30);
    loadingText.setFillColor(sf::Color::White);
    loadingText.setPosition(650, 700);

    // Main loop
    while (window.isOpen()) {
        // Handle events
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        // Clear the window
        window.clear();

        // Draw the background
        window.draw(background);

        // Draw the loading text
        window.draw(loadingText);

        // Display what was drawn
        window.display();

        // Return when everything is set up
        return;
    }
}

int videoplayer(sf::RenderWindow& window,const std::string& folder, int nb_images, const std::string& music) {
    sf::Music musique;
    musique.openFromFile(music);
    sf::Texture* textures = new sf::Texture[nb_images];
    for (int i = 0; i < nb_images; i+=2) // i++ au lieu de i += 2
    {
        std::string filename = folder + "/frame" + std::to_string(i) + ".png";
        if (!textures[i].loadFromFile(filename))
        {
            std::cout << "Erreur lors du chargement de l'image " << filename << std::endl;
            return -1;
        }
    }

    sf::Sprite sprite;
    int current_image = 0;
    float delay = 1.0f / 25.0f;
    float elapsed = 0.0f;
    sf::Clock clock;
    sf::Font font;
    font.loadFromFile("/Users/mac/cours/dev_app/assets/GUI/fonts/title.otf");

    sf::Text passMessage("M'en fou de ton histoire : SPACE", font);
    passMessage.setCharacterSize(25);
    passMessage.setFillColor(sf::Color::White);
    passMessage.setPosition(100, 700);

    if (!musique.openFromFile(music))
    {
        std::cerr << "Failed to load music file: " << music << std::endl;
        return -1;
    }



    musique.play();
    while (window.isOpen()){
        sf::Event event;

        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        elapsed += clock.restart().asSeconds();

        if (elapsed >= delay)
        {
            current_image = (current_image + 1) % nb_images;
            elapsed = 0.0f;
        }

        if (current_image % 2 == 0) // si current_image est pair
        {
            sprite.setTexture(textures[current_image]); // on utilise la texture correspondante
        }
        else // si current_image est impair
        {
            sprite.setTexture(textures[current_image - 1]); // on utilise la texture précédente
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
            //quitter la video
        }
        sprite.setScale((float)window.getSize().x / 720, (float)window.getSize().y / 404);
        window.clear();
        window.draw(sprite);
        window.draw(passMessage);
        window.display();


    }
    window.draw(passMessage);
    musique.stop();





        if (!waitForKey || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
            break;
        }


    delete[] textures;
    return 1;
}
*/