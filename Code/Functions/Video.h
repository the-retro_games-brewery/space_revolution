#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>


extern "C"
{
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
}

class Video
{

private:
    // Variables
    sf::Texture       m_Texture;
    bool              m_bVideoLoaded;
    bool              m_bImageNeedsUpdate;
    AVFormatContext*  m_pFormatCtx;
    sf::Uint8         m_iVideoStream;
    sf::Uint32        m_iFrameSize;
    AVCodecContext*   m_pCodecCtx;
    AVFrame*          m_pFrame;
    AVFrame*          m_pFrameRGB;
    sf::Uint8*        m_pBuffer;
    AVPacket          m_Packet;
    AVCodec*          m_pCodec;
    SwsContext*       img_convert_ctx;
    std::string       m_sFilename;
    float             m_fSecondsPerFrame;
    float             m_fTimePassedSinceLastFrameUpdate;
    sf::Clock         clock;

    // Functions
    void UpdateImage();
    void LoadNextFrame();
    void CloseVideo();

public:
    Video();
    explicit Video(const std::string& filename = "");
    ~Video();

    bool LoadFromFile(const std::string& filename);
    void Update(float time);
    void Update(sf::RenderWindow& window);

    sf::Vector2i Size() const { return sf::Vector2i(GetWidth(), GetHeight()); }
    unsigned int GetWidth() const { return m_pCodecCtx->width; }
    unsigned int GetHeight() const { return m_pCodecCtx->height; }
    float GetFrameRate() const { return 1 / m_fSecondsPerFrame; }
    sf::Color GetPixel(unsigned int x, unsigned int y) const;
};

