/*

#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>
extern "C" {
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
#include <libavcodec/avcodec.h>
#include <libavutil/time.h>
#include <libavutil/opt.h>
}

class Video {
public:
    Video();
    explicit Video(const std::string& filename);
    ~Video();

    bool LoadFromFile(const std::string& filename);
    void Update(float time);
    void Update(sf::RenderWindow& window);
    sf::Color GetPixel(unsigned int x, unsigned int y) const;

private:
    void LoadNextFrame();
    void UpdateImage();
    void CloseVideo();

    sf::Texture m_Texture;
    bool m_bVideoLoaded;
    bool m_bImageNeedsUpdate;
    AVFormatContext* m_pFormatCtx;
    int m_iVideoStream;
    int m_iFrameSize;
    AVCodecContext* m_pCodecCtx; // Modifier le type de cette variable
    AVFrame* m_pFrame;
    AVFrame* m_pFrameRGB;
    uint8_t* m_pBuffer;
    const AVCodec* m_pCodec; // Modifier le type de cette variable
    AVPacket m_Packet;
    struct SwsContext* img_convert_ctx;
    std::string m_sFilename;
    float m_fSecondsPerFrame;
    float m_fTimePassedSinceLastFrameUpdate;
    sf::Clock clock;
};

Video::Video() : 
    m_Texture(),
    m_bVideoLoaded(false),
    m_bImageNeedsUpdate(false),
    m_pFormatCtx(nullptr),
    m_iVideoStream(0),
    m_iFrameSize(0),
    m_pCodecCtx(nullptr),
    m_pFrame(nullptr),
    m_pFrameRGB(nullptr),
    m_pBuffer(nullptr), // Initialisez m_pBuffer à nullptr
    m_pCodec(nullptr),
    img_convert_ctx(nullptr),
    m_sFilename(""),
    m_fSecondsPerFrame(0),
    m_fTimePassedSinceLastFrameUpdate(0)
{
}

Video::Video(const std::string& filename) : 
    Video()
{
    if (!filename.empty())
    {
        if (!LoadFromFile(filename))
        {
            std::cout << "Could not load video!" << std::endl; // Probably should throw exception.
        }
    }
}

bool Video::LoadFromFile(const std::string& filename)
{
    CloseVideo();
    m_sFilename = filename;
    const char* const file = m_sFilename.c_str();

    if (avformat_open_input(&m_pFormatCtx, file, nullptr, nullptr) != 0)
    {
        std::cout << "Unexisting file!" << std::endl;
        return false;
    }

    if (avformat_find_stream_info(m_pFormatCtx, nullptr) < 0)
    {
        std::cout << "Couldn't find stream information!" << std::endl;
        return false;
    }

    av_dump_format(m_pFormatCtx, 0, file, 0);

    m_iVideoStream = -1;
    for (unsigned int i = 0; i < m_pFormatCtx->nb_streams; i++)
    {
        if (m_pFormatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            m_iVideoStream = i;
            break;
        }
    }

    if (m_iVideoStream == -1)
        return false;

    m_pCodecCtx = avcodec_alloc_context3(nullptr);
    if (!m_pCodecCtx)
    {
        std::cout << "Could not allocate codec context" << std::endl;
        return false;
    }

    avcodec_parameters_to_context(m_pCodecCtx, m_pFormatCtx->streams[m_iVideoStream]->codecpar);

    m_pCodec = avcodec_find_decoder(m_pCodecCtx->codec_id);

    if (m_pCodec == nullptr)
    {
        std::cout << "Unsupported codec!" << std::endl;
        return false;
    }

    if (avcodec_open2(m_pCodecCtx, m_pCodec, nullptr) < 0)
    {
        std::cout << "Could not open Codec Context" << std::endl;
        return false;
    }

    m_iFrameSize = m_pCodecCtx->width * m_pCodecCtx->height * 3;

    m_pFrame = av_frame_alloc();
    m_pFrameRGB = av_frame_alloc();

    if (m_pFrameRGB == nullptr || m_pFrame == nullptr)
    {
        std::cout << "Error allocating frame" << std::endl;
        return false;
    }

    int numBytes = av_image_get_buffer_size(AV_PIX_FMT_RGBA, m_pCodecCtx->width, m_pCodecCtx->height, 1);

    m_pBuffer = (uint8_t*)av_malloc(numBytes);

    av_image_fill_arrays(m_pFrameRGB->data, m_pFrameRGB->linesize, m_pBuffer, AV_PIX_FMT_RGBA,
                         m_pCodecCtx->width, m_pCodecCtx->height, 1);

    img_convert_ctx = sws_getCachedContext(nullptr, m_pCodecCtx->width, m_pCodecCtx->height,
                                           m_pCodecCtx->pix_fmt,
                                           m_pCodecCtx->width, m_pCodecCtx->height,
                                           AV_PIX_FMT_RGBA, SWS_FAST_BILINEAR,
                                           nullptr, nullptr, nullptr);
    m_bVideoLoaded = true;
    m_bImageNeedsUpdate = true;

    m_Texture.create(m_pCodecCtx->width, m_pCodecCtx->height);

    Update(10000);

    return true;
}

void Video::Update(float time)
{
    if (m_bVideoLoaded)
    {
        m_fTimePassedSinceLastFrameUpdate += time;
        UpdateImage();

        if (m_fTimePassedSinceLastFrameUpdate > m_fSecondsPerFrame)
        {
            m_fTimePassedSinceLastFrameUpdate = 0;
            LoadNextFrame();
        }
    }
}

void Video::LoadNextFrame()
{
    do
    {
        av_packet_unref(&m_Packet);
        int result = av_read_frame(m_pFormatCtx, &m_Packet);
        if (result < 0)
        {
            CloseVideo();
            LoadFromFile(m_sFilename);
            m_fTimePassedSinceLastFrameUpdate = 0;
            return;
        }
    } while (m_Packet.stream_index != m_iVideoStream);

    int frameFinished = 0;
    avcodec_send_packet(m_pCodecCtx, &m_Packet);
    avcodec_receive_frame(m_pCodecCtx, m_pFrame);
    frameFinished = 1;

    if (frameFinished)
    {
        sws_scale(img_convert_ctx, m_pFrame->data, m_pFrame->linesize,
                  0, m_pCodecCtx->height, m_pFrameRGB->data, m_pFrameRGB->linesize);
        m_bImageNeedsUpdate = true;
    }
}

sf::Color Video::GetPixel(unsigned int x, unsigned int y) const
{
    unsigned int i = 4 * (y * m_pCodecCtx->width + x);
    sf::Uint8 red = m_pFrameRGB->data[0][i];
    sf::Uint8 green = m_pFrameRGB->data[0][i + 1];
    sf::Uint8 blue = m_pFrameRGB->data[0][i + 2];
    return sf::Color(red, green, blue, 255);
}

void Video::UpdateImage()
{
    if (m_bImageNeedsUpdate)
    {
        m_Texture.update(m_pBuffer);
        m_bImageNeedsUpdate = false;
    }
}

void Video::CloseVideo()
{
    if (m_bVideoLoaded)
    {
        av_packet_unref(&m_Packet);
        if (m_pBuffer != nullptr) // Vérifiez si m_pBuffer est différent de nullptr
        {
            av_free(m_pBuffer);
            m_pBuffer = nullptr; // Optionnel : Définissez m_pBuffer à nullptr après la libération
        }
        av_frame_free(&m_pFrameRGB);
        av_frame_free(&m_pFrame);
        avcodec_free_context(&m_pCodecCtx);
        avformat_close_input(&m_pFormatCtx);
        sws_freeContext(img_convert_ctx);

        m_bVideoLoaded = false;
        m_bImageNeedsUpdate = false;
    }
}


void Video::Update(sf::RenderWindow& window)
{
    float deltaTime = clock.restart().asSeconds();

    if (m_bVideoLoaded)
    {
        m_fTimePassedSinceLastFrameUpdate += deltaTime;

        if (m_fTimePassedSinceLastFrameUpdate > m_fSecondsPerFrame)
        {
            LoadNextFrame();
            m_fTimePassedSinceLastFrameUpdate = 0;
        }

        UpdateImage(); // This will update the texture if a new frame is loaded
        window.clear();
        sf::Sprite sprite(m_Texture);
        window.draw(sprite);
        window.display();
    }
}

Video::~Video()
{
    CloseVideo();
}

int main() {
    // Create SFML Window
    sf::RenderWindow window(sf::VideoMode(800, 600), "SFML Video Player");

    // Create Video object
    Video video("rickrolld.mp4");

    // Main loop
    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        // Update the video player
        video.Update(window);
    }

    return 0;
}

// g++ -std=c++11 -I/usr/local/include -L/usr/local/lib -lavformat -lavcodec -lswscale -lavutil -lsfml-graphics -lsfml-window -lsfml-system VideoPlayer1.cpp -o VideoPlayer1
*/