#include "Fonctions.h"

int gamePause(sf::RenderWindow& window) {
    sf::Texture backgroundTexture;
    sf::Texture playButtonTexture;
    sf::Texture playButtonTextureClicked;
    sf::Texture titleTexture;
    sf::Texture menuButtonTexture;
    sf::Texture menuButtonTextureClicked;

    sf::Music music;

    if (!backgroundTexture.loadFromFile("../assets/GUI/pauseImage.jpg")) {
        std::cout << "Problème lors du chargement de la texture du fond" << std::endl;
    }
    if (!playButtonTexture.loadFromFile("../assets/GUI/playButton.png")) {
        std::cout << "Problème lors du chargement de la texture du bouton de démarrage" << std::endl;
    }
    if (!playButtonTextureClicked.loadFromFile("../assets/GUI/playButtonClicked.png")) {
        std::cout << "Problème lors du chargement de la texture du bouton de démarrage" << std::endl;
    }
    if (!menuButtonTexture.loadFromFile("../assets/GUI/menuButton.png")) {
        std::cout << "Problème lors du chargement de la texture du bouton de musique" << std::endl;
    }
    if (!menuButtonTextureClicked.loadFromFile("../assets/GUI/menuButotnClicked.png")) {
        std::cout << "Problème lors du chargement de la texture du bouton de musique" << std::endl;
    }
    music.openFromFile("../Sounds/inGame/Du Hast (8-Bit Rammstien Emulation).mp3");

    sf::Sprite backgroundSprite(backgroundTexture);
    sf::Sprite playButtonSprite(playButtonTexture);
    sf::Sprite quitButtonSprite(menuButtonTexture);

    float ButtonScaleFactor = 0.5f;
    playButtonSprite.setScale(ButtonScaleFactor, ButtonScaleFactor);
    quitButtonSprite.setScale(ButtonScaleFactor, ButtonScaleFactor);

    backgroundSprite.setScale(
            static_cast<float>(window.getSize().x) / backgroundTexture.getSize().x,
            static_cast<float>(window.getSize().y) / backgroundTexture.getSize().y
    );

    quitButtonSprite.setPosition(530, 400);
    playButtonSprite.setPosition(530, 200);

    music.play();
    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
                return 0;
            } else if (event.type == sf::Event::MouseButtonPressed) {
                sf::Vector2i mousePosition = sf::Mouse::getPosition(window);
                sf::FloatRect playButtonBounds = playButtonSprite.getGlobalBounds();
                sf::FloatRect quitButtonBounds = quitButtonSprite.getGlobalBounds();
                if (playButtonBounds.contains(mousePosition.x, mousePosition.y)) {
                    playButtonSprite.setTexture(playButtonTextureClicked);
                    return 1;
                }
                if (quitButtonBounds.contains(mousePosition.x, mousePosition.y)) {
                    quitButtonSprite.setTexture(menuButtonTextureClicked);
                    return 2;
                }
            } else if (event.type == sf::Event::MouseMoved) {
                sf::Vector2i mousePosition = sf::Mouse::getPosition(window);
                sf::FloatRect playButtonBounds = playButtonSprite.getGlobalBounds();
                sf::FloatRect quitButtonBounds = quitButtonSprite.getGlobalBounds();

                if (playButtonBounds.contains(mousePosition.x, mousePosition.y)) {
                    playButtonSprite.setTexture(playButtonTextureClicked);
                } else {
                    playButtonSprite.setTexture(playButtonTexture);
                }
                if (quitButtonBounds.contains(mousePosition.x, mousePosition.y)) {
                    quitButtonSprite.setTexture(menuButtonTextureClicked);
                } else {
                    quitButtonSprite.setTexture(menuButtonTexture);
                }

            } else if (event.type == sf::Event::KeyPressed) {
                if (event.key.code == sf::Keyboard::Z) {
                    std::cout << "Z key pressed: Move up\n";
                } else if (event.key.code == sf::Keyboard::S) {
                    std::cout << "S key pressed: Move down\n";
                }
            }
        }

        window.clear();

        backgroundSprite.setScale(
            static_cast<float>(window.getSize().x) / backgroundTexture.getSize().x,
            static_cast<float>(window.getSize().y) / backgroundTexture.getSize().y
        );

        window.draw(backgroundSprite);
        window.draw(playButtonSprite);
        window.draw(quitButtonSprite);

        window.display();
    }
    music.stop();
    return 0;
}
