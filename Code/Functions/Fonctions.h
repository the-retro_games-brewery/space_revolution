#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <cmath>
#include <iostream>
#include <sstream>

/**
 * @brief Affiche le menu principal du jeu.
 * 
 * @param window La fenêtre SFML où afficher le menu.
 * @return Le choix du joueur.
 */
int gameMenu(sf::RenderWindow& window);

/**
 * @brief Affiche le menu de pause du jeu.
 * 
 * @param window La fenêtre SFML où afficher le menu.
 * @return Le choix du joueur.
 */
int gamePause(sf::RenderWindow& window);

/**
 * @brief Affiche l'écran de fin de partie.
 * 
 * @param window La fenêtre SFML où afficher l'écran de fin de partie.
 * @return Le choix du joueur.
 */
int gameOver(sf::RenderWindow& window);
