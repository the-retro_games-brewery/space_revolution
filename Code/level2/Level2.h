#ifndef LEVEL2_H
#define LEVEL2_H



#include "../Aux/aux.h"
#include "../Aux/Objet.h"
#include "../Aux/Personnage.h"


class Niveau2 {
private:
    int tailleMapX = 40;
    int tailleMapY = 30;
    std::vector<Objet> jeuMatrix;
    std::vector<Objet> decorLayer1;
    std::vector<Objet> decorLayer2;
    std::vector<Objet> decorLayer3;
    std::vector<Personnage> ennemis;
    Personnage joueur;
    char entree;

    std::time_t dernierEnnemi; // Moment de la dernière création d'ennemi
    int tempsEntreEnnemis;
    
    std::time_t debutPartie; // Déclaration de la variable debutPartie
    std::time_t dureePartie; // Durée de la partie

    std::time_t dernierDecor1; // Pour suivre le moment de la création du dernier décor
    std::time_t dernierDecor2; // Pour suivre le moment de la création du dernier décor
    std::time_t dernierDecor3; // Pour suivre le moment de la création du dernier décor
    double tempsEntreDecors; // Temps entre chaque création de décor en secondes
    int boost;

public:
    Niveau2(Personnage joueur);
    ~Niveau2();
    
    int getBoost() const {
        return boost;
    }

    void setBoost(int newBoost) {
        boost = newBoost;
    }

    void useboost(){
        boost -=1;
    }
    void afficher();
    int getTailleMapX() const;
    int getTailleMapY() const;
    const std::vector<Personnage>& getEnnemis() const;
    const std::vector<Objet>& getDecorLayer1() const;
    const std::vector<Objet>& getDecorLayer2() const;
    const std::vector<Objet>& getDecorLayer3();
    Personnage& getJoueur();

    int score = 0;

    void creerEnnemi();
    void creerDecorLayer1();
    void creerDecorLayer2();
    void creerDecorLayer3();
    int mettreAJour();


    void viderEnnemis();

void viderDecorLayer1();

void viderDecorLayer2();

void viderDecorLayer3();

void viderBallesJoueur();

void viderBallesEnnemis();
};


#endif // LEVEL2_H