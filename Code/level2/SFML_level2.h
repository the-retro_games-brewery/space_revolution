#ifndef SFML_LEVEL2_H
#define SFML_LEVEL2_H

#include "Level2.h"

/**
 * @class SFML_level2
 * @brief Classe gérant l'affichage et le fonctionnement du deuxième niveau du jeu en utilisant la bibliothèque SFML.
 */
class SFML_level2 {
private:
    Niveau2 niveau; /**< Instance du deuxième niveau du jeu */
    sf::RenderWindow& window; /**< Référence vers la fenêtre SFML */

    // Textures des éléments du jeu
    sf::Texture playerTexture; /**< Texture du joueur */
    sf::Texture Background; /**< Texture du fond d'écran */
    sf::Texture enemi1Texture; /**< Texture de l'ennemi 1 */
    sf::Texture enemi2Texture; /**< Texture de l'ennemi 2 */
    sf::Texture enemi3Texture; /**< Texture de l'ennemi 3 */
    sf::Texture playerbulletTexture; /**< Texture de la balle du joueur */
    sf::Texture enemibulletTexture; /**< Texture de la balle de l'ennemi */
    sf::Texture decorLayer1Texture; /**< Texture de la première couche de décor */
    sf::Texture decorLayer2Texture; /**< Texture de la deuxième couche de décor */
    sf::Texture decorLayer3Texture; /**< Texture de la troisième couche de décor */

    // Variables de frames et d'horloge pour le joueur et les ennemis
    const int playerFrameWidth = 190; /**< Largeur d'une frame du joueur */
    const int playerFrameHeight = 73; /**< Hauteur d'une frame du joueur */
    const int playerFrameCount = 8; /**< Nombre total de frames pour l'animation du joueur */
    int playerFrameIndex = 0; /**< Index de la frame actuelle du joueur */
    int playerWidth = playerFrameWidth; /**< Largeur du joueur */
    int playerHeight = playerFrameHeight; /**< Hauteur du joueur */
    sf::Clock playerClock; /**< Horloge pour l'animation du joueur */

    const int enemiFrameWidth = 188; /**< Largeur d'une frame de l'ennemi */
    const int enemiFrameHeight = 73; /**< Hauteur d'une frame de l'ennemi */
    const int enemiFrameCount = 8; /**< Nombre total de frames pour l'animation de l'ennemi */
    int enemiFrameIndex = 0; /**< Index de la frame actuelle de l'ennemi */
    sf::Clock enemiClock; /**< Horloge pour l'animation de l'ennemi */

    std::vector<int> ballesFrameIndex; /**< Index de la frame actuelle pour chaque balle */
    std::vector<int> ballesFrameCount; /**< Nombre total de frames pour chaque balle */
    std::vector<sf::Clock> ballesClocks; /**< Horloge pour l'animation de chaque balle */
    int initBoost; /**< Bonus d'initialisation */

public:
    /**
     * @brief Constructeur de la classe SFML_level2.
     * @param window La fenêtre SFML où afficher le niveau.
     */
    SFML_level2(sf::RenderWindow& window);

    /**
     * @brief Destructeur de la classe SFML_level2.
     */
    ~SFML_level2();

    /**
     * @brief Charge les textures nécessaires pour le niveau.
     */
    void TextureLoader();

    /**
     * @brief Met à jour les frames des éléments animés du jeu.
     */
    void updateFrames();

    /**
     * @brief Affiche le niveau dans la fenêtre SFML.
     */
    void Render();

    /**
     * @brief Nettoie les ressources utilisées par le niveau.
     */
    void clean();

    /**
     * @brief Lance le deuxième niveau du jeu.
     * @return Le résultat du jeu.
     */
    int run(); // Run the game
};

#endif // SFML_LEVEL2_H
