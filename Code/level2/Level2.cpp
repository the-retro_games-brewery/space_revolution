#include "Level2.h"


Niveau2::Niveau2(Personnage joueur) : joueur(joueur), dernierEnnemi(std::time(nullptr)), dernierDecor1(std::time(nullptr)), dernierDecor2(std::time(nullptr)), dernierDecor3(std::time(nullptr)), tempsEntreEnnemis(1) {
boost = 4;
}


Niveau2::~Niveau2() {
    viderBallesEnnemis();
    viderBallesJoueur();
    viderEnnemis();
    viderDecorLayer1();
    viderDecorLayer2();
    viderDecorLayer3();
}

const std::vector<Personnage>& Niveau2::getEnnemis() const {
    return ennemis;
}

const std::vector<Objet>& Niveau2::getDecorLayer1() const {
    return decorLayer1;
}

const std::vector<Objet>& Niveau2::getDecorLayer2() const {
    return decorLayer2;
}

const std::vector<Objet>& Niveau2::getDecorLayer3() {
    return decorLayer3;
}

Personnage& Niveau2::getJoueur() {
    return joueur;
}

int Niveau2::getTailleMapX() const {
    return tailleMapX;
}
int Niveau2::getTailleMapY() const {
    return tailleMapY;
}

void Niveau2::creerEnnemi() {
    int y = rand() % (tailleMapY - 2) + 1; // Coordonnée Y aléatoire
    int x = tailleMapX - 1; // Coordonnée X fixe (à droite de l'écran)

    // Vérifier que l'ennemi ne dépasse pas en dehors de l'écran
    if (y + 1 < tailleMapY) {
        ennemis.push_back(Personnage(x, y, 1, 1)); // Ajouter l'ennemi
    }
}

// Ajoutez cette fonction à la classe Niveau2 pour créer un décor et le déplacer
void Niveau2::creerDecorLayer1()  {
    // Créez un nouvel objet de décor à partir du bord droit de l'écran
    int y = tailleMapY - 1; // Position verticale fixe tout en bas
    int x = tailleMapX - 1; // Position horizontale à partir du bord droit

    // Ajoutez l'objet de décor à la première couche de décor
    decorLayer1.push_back(Objet(x, y, 1));
    }


void Niveau2::creerDecorLayer2() {
    // Créez un nouvel objet de décor à partir du bord droit de l'écran
    int y = tailleMapY - 1; // Position verticale fixe tout en bas
    int x = tailleMapX - 1; // Position horizontale à partir du bord droit

    // Ajoutez l'objet de décor à la deuxième couche de décor
    decorLayer2.push_back(Objet(x, y, 2));
}

void Niveau2::creerDecorLayer3() {
    // Créez un nouvel objet de décor à partir du bord droit de l'écran
    int y = tailleMapY - 1; // Position verticale fixe tout en bas
    int x = tailleMapX - 1; // Position horizontale à partir du bord droit

    // Ajoutez l'objet de décor à la troisième couche de décor
    decorLayer3.push_back(Objet(x, y, 3));
}



void Niveau2::viderEnnemis() {
    ennemis.clear();
}

void Niveau2::viderDecorLayer1() {
    decorLayer1.clear();
}

void Niveau2::viderDecorLayer2() {
    decorLayer2.clear();
}

void Niveau2::viderDecorLayer3() {
    decorLayer3.clear();
}

void Niveau2::viderBallesJoueur() {
    joueur.viderBalles();
}
void Niveau2::viderBallesEnnemis(){
    for (auto& ennemi : ennemis) {
    ennemi.viderBalles();
    }
}

int Niveau2::mettreAJour() {
    std::time_t maintenant = std::time(nullptr);

    // Calcul de la différence de temps depuis la dernière création d'ennemi et de décor
    double dureeDepuisDernierEnnemi = std::difftime(maintenant, dernierEnnemi);
    double dureeDepuisDernierDecor1 = std::difftime(maintenant, dernierDecor1);
    double dureeDepuisDernierDecor2 = std::difftime(maintenant, dernierDecor2);
    double dureeDepuisDernierDecor3 = std::difftime(maintenant, dernierDecor3);

    // Vérification de la condition de défaite
    if (!joueur.isAlive()) {
        std::cout << "Vous avez perdu !" << std::endl;
        return 1;
    }

    // Vérification de la condition de victoire
    if (score == 10) {
        std::cout << "Vous avez gagné !" << std::endl;
        return 2;
    }

    // Vérification du temps écoulé depuis la création du dernier ennemi et du dernier décor
    if (dureeDepuisDernierEnnemi > tempsEntreEnnemis) {
        creerEnnemi();
        dernierEnnemi = maintenant;
    }
    if (dureeDepuisDernierDecor1 > tempsEntreDecors) {
        creerDecorLayer1();
        dernierDecor1 = maintenant;
    }
    if (dureeDepuisDernierDecor2 > tempsEntreDecors) {
        creerDecorLayer2();
        dernierDecor2 = maintenant;
    }
    if (dureeDepuisDernierDecor3 > tempsEntreDecors) {
        creerDecorLayer3();
        dernierDecor3 = maintenant;
    }


    for (auto& ennemi : ennemis) {
        if (ennemi.getX() > 0)
            ennemi.setX(ennemi.getX() - ennemi.getVitesse());
    }
    for (auto& decor : decorLayer1) {
        if (decor.getX() > 0)
            decor.setX(decor.getX() - decor.getVitesse());
    }
    for (auto& decor : decorLayer2) {
        if (decor.getX() > 0)
            decor.setX(decor.getX() - decor.getVitesse());
    }
    for (auto& decor : decorLayer3) {
        if (decor.getX() > 0)
            decor.setX(decor.getX() - decor.getVitesse());
    }

    // Gestion des collisions entre les balles et les ennemis
    for (auto& ennemi : ennemis) {
        for (auto& balle : joueur.getBalles()) {
            if (balle.getX() == ennemi.getX() && balle.getY() == ennemi.getY()) {
                ennemi.degat(1);
                std::cout << "Ennemi touché par une balle !" << std::endl;
            }
        }
    }

    // Gestion des collisions entre le joueur et les ennemis
    for (auto& ennemi : ennemis) {
        if (joueur.getX() == ennemi.getX() && joueur.getY() == ennemi.getY()) {
            joueur.degat(1);
            std::cout << "Joueur touché par un ennemi !" << std::endl;
        }
    }

    // Les ennemis tirent aléatoirement
    int chance = rand() % 100;
    for (auto& ennemi : ennemis) {
        if (chance < 5) {
            ennemi.tirerBalle(0,0, 2);
        }
    }
    

    // Suppression des ennemis morts
    for (auto it = ennemis.begin(); it != ennemis.end();) {
        if (!it->isAlive()) {
            it = ennemis.erase(it);
            std::cout << "Ennemi mort !" << std::endl;
            score += 1;
        } else {
            ++it;
        }
    }

    // Gestion des collisions entre les balles ennemies et le joueur
    for (auto& ennemi : ennemis) {
        for (auto& balle : ennemi.getBalles()) {
            if (balle.getX() == joueur.getX() && balle.getY() == joueur.getY()) {
                joueur.degat(1);
                std::cout << "Joueur touché par une balle ennemie !" << std::endl;
                
            }
        }
    }

    joueur.updateBalles(1,0);
    // Mise à jour des positions des balles ennemies
    for (auto& ennemi : ennemis) {
        ennemi.updateBalles(-1,0);
    }

    // Suppression des balles qui sortent de l'écran
    for (auto it = joueur.getBalles().begin(); it != joueur.getBalles().end();) {
        if (it->getX() < 0 || it->getX() >= tailleMapX || it->getY() < 0 || it->getY() >= tailleMapY) {
            joueur.supprimerBalle(*it);
            std::cout << "Balle sortie de l'écran !" << std::endl;
        } else {
            ++it;
        }
    }
    for (auto& ennemi : ennemis) {
        for (auto it = ennemi.getBalles().begin(); it != ennemi.getBalles().end();) {
            if (it->getX() < 0 || it->getX() >= tailleMapX || it->getY() < 0 || it->getY() >= tailleMapY) {
                ennemi.supprimerBalle(*it);
                std::cout << "Balle ennemie sortie de l'écran !" << std::endl;
            } else {
                ++it;
            }
        }
    }


    // Suppression des décors qui sortent de l'écran
    for (auto it = decorLayer1.begin(); it != decorLayer1.end();) {
        if (it->getX() < 0) {
            it = decorLayer1.erase(it);
            std::cout << "Décor Layer 1 sorti de l'écran !" << std::endl;
        } else {
            ++it;
        }
    }
    for (auto it = decorLayer2.begin(); it != decorLayer2.end();) {
        if (it->getX() < 0) {
            it = decorLayer2.erase(it);
            std::cout << "Décor Layer 2 sorti de l'écran !" << std::endl;
        } else {
            ++it;
        }
    }
    for (auto it = decorLayer3.begin(); it != decorLayer3.end();) {
        if (it->getX() < 0) {
            it = decorLayer3.erase(it);
            std::cout << "Décor Layer 3 sorti de l'écran !" << std::endl;
        } else {
            ++it;
        }
    }

    

    if(score == 10){
        return 2;
    }
    if(!joueur.isAlive()){
        return 1;
    }
    // Le jeu continue
    return 0;
}
