#include "SFML_level2.h"

SFML_level2::SFML_level2(sf::RenderWindow& window)
        : window(window), niveau(Personnage(5, 5, 10, 1)) {
    TextureLoader();
}

SFML_level2::~SFML_level2() {
    clean();
    window.~RenderWindow();
}

void SFML_level2::TextureLoader() {
    if (!playerTexture.loadFromFile("../assets/Starships/Scoller/ship/P1/ship.png")) {
        std::cout << "Problème lors du chargement de la texture du joueur" << std::endl;
    }
    if (!Background.loadFromFile("../assets/Backgrounds/Scrolling/Scrolling1.png")) {
        std::cout << "Problème lors du chargement de la texture du fond" << std::endl;
    }
    if (!enemi1Texture.loadFromFile("../assets/Starships/Scoller/Enemi/Ship4/enemy2.png")) {
        std::cout << "Problème lors du chargement de la texture de l'ennemi" << std::endl;
    }
    if (!enemi2Texture.loadFromFile("../assets/Starships/Scoller/Enemi/Ship4/enemy2.png")) {
        std::cout << "Problème lors du chargement de la texture de l'ennemi" << std::endl;
    }
    if (!enemi3Texture.loadFromFile("../assets/Starships/Scoller/Enemi/Ship4/enemy2.png")) {
        std::cout << "Problème lors du chargement de la texture de l'ennemi" << std::endl;
    }
    if (!playerbulletTexture.loadFromFile("../assets/Shots/Shot1/shot1_4.png")) {
        std::cout << "Problème lors du chargement de la texture de la balle" << std::endl;
    }

    if (!enemibulletTexture.loadFromFile("../assets/Shots/Shot5/shot5_4.png")) {
        std::cout << "Problème lors du chargement de la texture de la balle" << std::endl;
    }
    if (!decorLayer1Texture.loadFromFile("../assets/Backgrounds/Scrolling/back_1.png")) {
        std::cout << "Problème lors du chargement de la texture du décor" << std::endl;
    }
    if (!decorLayer2Texture.loadFromFile("../assets/Backgrounds/Scrolling/front2.png")) {
        std::cout << "Problème lors du chargement de la texture du décor" << std::endl;
    }
    if (!decorLayer3Texture.loadFromFile("../assets/Backgrounds/Scrolling/back_3.png")) {
        std::cout << "Problème lors du chargement de la texture du décor" << std::endl;
    }

}

void SFML_level2::updateFrames() {
    // Met à jour le cadre du joueur
    if (playerClock.getElapsedTime().asSeconds() > 0.1) {
        playerFrameIndex = (playerFrameIndex + 1) % playerFrameCount;
        playerClock.restart();
    }

    // Met à jour le cadre de l'ennemi
    if (enemiClock.getElapsedTime().asSeconds() > 0.1) {
        enemiFrameIndex = (enemiFrameIndex + 1) % enemiFrameCount;
        enemiClock.restart();
    }
}


void SFML_level2::Render() {

    int tailleTuileX = window.getSize().x / niveau.getTailleMapX();
    int tailleTuileY = window.getSize().y / niveau.getTailleMapY();
    window.clear();
    // Dessiner le fond
    static float backgroundOffset = 0.0f; // Définir une variable statique pour conserver la position du fond
    backgroundOffset += 0.8f; // Vitesse de défilement du fond, ajustez selon vos besoins

    // Dessiner le fond avec la position modifiée
    sf::Sprite backgroundSprite(Background);
    backgroundSprite.setPosition(-backgroundOffset, 0); // Modifier la position horizontale seulement
    window.draw(backgroundSprite);
    // Dessiner le joueur
    sf::Sprite playerSprite(playerTexture);
    playerSprite.setTextureRect(sf::IntRect(playerFrameIndex * playerFrameWidth, 0, playerFrameWidth, playerFrameHeight));
    playerSprite.setPosition(niveau.getJoueur().getX() * tailleTuileX, niveau.getJoueur().getY() * tailleTuileY);
    window.draw(playerSprite);

    for (auto& ennemi : niveau.getEnnemis()) {
        if (ennemi.getPointsDeVie()>0 && ennemi.getX() > 0 && ennemi.getY() > 0) {
            sf::Sprite enemySprite(enemi1Texture); // Utilisation de enemi1Texture pour l'exemple
            enemySprite.setTextureRect(sf::IntRect(enemiFrameIndex * enemiFrameWidth, 0, enemiFrameWidth, enemiFrameHeight));
            enemySprite.setPosition(ennemi.getX() * tailleTuileX , ennemi.getY() * tailleTuileY );
            window.draw(enemySprite);
        }
    }

    // Dessiner les balles
    for (auto& balle : niveau.getJoueur().getBalles()) {
        sf::Sprite bulletSprite(playerbulletTexture);
        bulletSprite.scale(2, 2);
        bulletSprite.setPosition(balle.getX() * tailleTuileX, balle.getY() * tailleTuileY);
        bulletSprite.move(140,0);
        window.draw(bulletSprite);
    }
    
    // Dessiner les balles ennemi
    for (auto& ennemi : niveau.getEnnemis()) {
        for (auto& balle : ennemi.getBalles()) {
            sf::Sprite bulletSprite(enemibulletTexture);
            bulletSprite.scale(1.5, 1.5);
            bulletSprite.setPosition(balle.getX() * tailleTuileX, balle.getY() * tailleTuileY);
            bulletSprite.move(-140,0);
            window.draw(bulletSprite);
        }
    }
    
    


    // Dessiner les décors
    for (auto& decor : niveau.getDecorLayer1()) {
        sf::Sprite decorSprite(decorLayer1Texture);
        decorSprite.setPosition(decor.getX() * tailleTuileX, decor.getY() * tailleTuileY);
                decorSprite.move(0,-560);
        window.draw(decorSprite);
    }
    for (auto& decor : niveau.getDecorLayer2()) {
        sf::Sprite decorSprite(decorLayer2Texture);
        decorSprite.setPosition(decor.getX() * tailleTuileX, decor.getY() * tailleTuileY);
                decorSprite.move(0,-600);
        window.draw(decorSprite);
    }
    for (auto& decor : niveau.getDecorLayer3()) {
        sf::Sprite decorSprite(decorLayer3Texture);
        decorSprite.setPosition(decor.getX() * tailleTuileX, decor.getY() * tailleTuileY);
                decorSprite.move(0,-600);
        window.draw(decorSprite);
    }
    

        sf::RectangleShape healthBar(sf::Vector2f(niveau.getJoueur().getPointsDeVie() * 10, 10));
    healthBar.setFillColor(sf::Color::Green);
    healthBar.setPosition(10, 10);
    window.draw(healthBar);

    // Dessiner la barre de boost
    sf::RectangleShape boostBar(sf::Vector2f(niveau.getBoost() * 10, 10));
    boostBar.setFillColor(sf::Color::Blue);
    boostBar.setPosition(10, 30);
    window.draw(boostBar);

    window.display();
}

void SFML_level2::clean(){
        // Libérer la mémoire une fois le niveau fini
    niveau.viderEnnemis();
    niveau.viderDecorLayer1();
    niveau.viderDecorLayer2();
    niveau.viderDecorLayer3();
    niveau.viderBallesEnnemis();
    niveau.viderBallesJoueur();
    playerTexture.~Texture(); // Libère la mémoire de la texture du joueur
    Background.~Texture(); // Libère la mémoire de la texture du fond
    enemi1Texture.~Texture(); // Libère la mémoire de la texture de l'ennemi 1
    enemi2Texture.~Texture(); // Libère la mémoire de la texture de l'ennemi 2
    enemi3Texture.~Texture(); // Libère la mémoire de la texture de l'ennemi 3
    playerbulletTexture.~Texture(); // Libère la mémoire de la texture de la balle du joueur
    enemibulletTexture.~Texture(); // Libère la mémoire de la texture de la balle ennemie
    decorLayer1Texture.~Texture(); // Libère la mémoire de la texture du décor de la couche 1
    decorLayer2Texture.~Texture(); // Libère la mémoire de la texture du décor de la couche 2
    decorLayer3Texture.~Texture(); // Libère la mémoire de la texture du décor de la couche 3
    
}
int SFML_level2::run() {
    sf::Clock clock; // Créez une horloge pour mesurer le temps écoulé
    float tempsEntreMisesAJour = 0.1f; // Temps entre les mises à jour en secondes
    sf::Music music;
    
    sf::String musicFile1= "../Sounds/inGame/Wake Up.mp3";
    sf::String musicFile2= "../Sounds/inGame/Lowlife (8-Bit Computer Game Version).mp3";
    sf::String musicFile3= "../Sounds/inGame/Kashmir.mp3";

    if (rand() % 3 + 1 == 1) {
        if (!music.openFromFile(musicFile1)) {
            std::cout << "Erreur lors du chargement de la musique" << std::endl;
        }
    }
    else if (rand() % 3 + 1 == 2) {
        if (!music.openFromFile(musicFile2)) {
            std::cout << "Erreur lors du chargement de la musique" << std::endl;
        }
    }
    else {
        if (!music.openFromFile(musicFile3)) {
            std::cout << "Erreur lors du chargement de la musique" << std::endl;
        }
    }
    music.play();
    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                    music.stop();
                    clean();
                std::cout << "Au revoir ";
                window.close();
                
            }


    initBoost +=1;

    if(initBoost >= 28){
        niveau.setBoost(4);
        initBoost = 0;
    }
            // Vérifiez les événements de touche pressée
            if (event.type == sf::Event::KeyPressed) {
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) && (event.key.code == sf::Keyboard::Z) &&(niveau.getBoost() > 0)) {
                                            niveau.getJoueur().setY(niveau.getJoueur().getY() - niveau.getJoueur().getVitesse()*2);
                                            niveau.useboost();
                }
                                if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) && (event.key.code == sf::Keyboard::S) &&(niveau.getBoost() > 0)) {
                                            niveau.getJoueur().setY(niveau.getJoueur().getY() + niveau.getJoueur().getVitesse()*2);
                                            niveau.useboost();
                }
                if (event.key.code == sf::Keyboard::Z) {
                    if (niveau.getJoueur().getY() > 0) {
                        niveau.getJoueur().setY(niveau.getJoueur().getY() - niveau.getJoueur().getVitesse());
                        std::cout << "Le joueur monte" << std::endl;
                    }
                } else if (event.key.code == sf::Keyboard::S) {
                    if (niveau.getJoueur().getY() < window.getSize().x - 1) {
                        niveau.getJoueur().setY(niveau.getJoueur().getY() + niveau.getJoueur().getVitesse());
                        std::cout << "Le joueur descend" << std::endl;
                    }
                } else if (event.key.code == sf::Keyboard::Q) {
                    if (niveau.getJoueur().getX() > 0) {
                        niveau.getJoueur().setX(niveau.getJoueur().getX() - niveau.getJoueur().getVitesse());
                        std::cout << "Le joueur va à gauche" << std::endl;
                    }
                } else if (event.key.code == sf::Keyboard::D) {
                    if (niveau.getJoueur().getX() < window.getSize().y - 1) {
                        niveau.getJoueur().setX(niveau.getJoueur().getX() + niveau.getJoueur().getVitesse());
                        std::cout << "Le joueur va à droite" << std::endl;
                    }
                } else if (event.key.code == sf::Keyboard::Space) {
                    niveau.getJoueur().tirerBalle(niveau.getJoueur().getX() + 1, niveau.getJoueur().getY(), 1);
                    std::cout << "Le joueur tire une balle vers la droite" << std::endl;
                }

                else if (event.key.code == sf::Keyboard::Escape) {
                    music.pause();
                    gamePause(window);
                    music.play();
                }
            }
        }
        // Vérifiez si le temps écoulé dépasse le temps entre les mises à jour
        if (clock.getElapsedTime().asSeconds() >= tempsEntreMisesAJour) {
            // Réinitialisez l'horloge pour mesurer le temps écoulé jusqu'à la prochaine mise à jour
            clock.restart();

                int gameState = niveau.mettreAJour();

                if (gameState == 2) {
                    std::cout << "Vous avez gagné !" << std::endl;
                    return 2;
                } else if (gameState == 1) {
                    std::cout << "Vous avez perdu !" << std::endl;
                    return 1;
                }
            updateFrames();
            Render();
        }


    }

    window.close();
    return 0;
}

/*
int main(){
    sf::RenderWindow window(sf::VideoMode(1200, 900), "SFML Level 2");
    SFML_level2 game(window);
    game.run();
    return 0;
}
*/

//g++ -std=c++17 SFML_level2.cpp level2.cpp ../Aux/Personnage.cpp ../Aux/Objet.cpp ../Aux/Coordonnees.cpp ../Aux/Bullet.cpp ../Functions/pause.cpp -o testGLV2 -lsfml-graphics -lsf ml-window -lsfml-system -lsfml-audio