/**
 * @file Bullet.h
 * @brief Définition de la classe Bullet.
 * @date 03/04/2024
 */

#ifndef CODE_BULLET_H
#define CODE_BULLET_H

#include "aux.h"
#include "Objet.h"
#include "Coordonnees.h"

/**
 * @class Bullet
 * @brief Classe représentant une balle tirée par un personnage.
 */
class Bullet : public Objet {
private:
    int puissance; /**< Puissance de la balle */
    std::string type; /**< Type de la balle */
    int vitesse; /**< Vitesse de déplacement de la balle */

public:
    /**
     * @brief Constructeur de la classe Bullet.
     * @param x Position horizontale de la balle.
     * @param y Position verticale de la balle.
     * @param _puissance Puissance de la balle.
     * @param _type Type de la balle.
     * @param _vitesse Vitesse de déplacement de la balle.
     */
    Bullet(int x, int y, int _puissance, std::string _type, int _vitesse);

    /**
     * @brief Destructeur de la classe Bullet.
     */
    ~Bullet();

    // Méthodes d'accès
    /**
     * @brief Obtenir la puissance de la balle.
     * @return La puissance de la balle.
     */
    int getPuissance() const;

    /**
     * @brief Obtenir le type de la balle.
     * @return Le type de la balle.
     */
    std::string getType() const;

    // Méthodes de modification
    /**
     * @brief Modifier la puissance de la balle.
     * @param _puissance La nouvelle puissance de la balle.
     */
    void setPuissance(int _puissance);

    /**
     * @brief Modifier le type de la balle.
     * @param _type Le nouveau type de la balle.
     */
    void setType(std::string _type);

    /**
     * @brief Déplacer la balle dans une direction donnée.
     * @param x La nouvelle position horizontale de la balle.
     * @param y La nouvelle position verticale de la balle.
     */
    void move(int x, int y);

    /**
     * @brief Déplacer la balle vers les coordonnées spécifiées.
     * @param x La position horizontale cible.
     * @param y La position verticale cible.
     */
    void deplacerAB(int x, int y);

    /**
     * @brief Gérer la collision de la balle avec un autre objet.
     */
    void collision() const;

    /**
     * @brief Surcharge de l'opérateur d'égalité.
     * @param other L'objet Bullet à comparer.
     * @return true si les balles sont identiques, sinon false.
     */
    bool operator==(const Bullet& other) const {
        // Comparer les attributs pertinents des balles
        return (getX() == other.getX()) && (getY() == other.getY()) && (puissance == other.puissance) && (type == other.type);
    }
};

#endif // CODE_BULLET_H
