/**
 * @file Objet.h
 * @brief Définition de la classe Objet.
 */

#ifndef CODE_OBJET_H
#define CODE_OBJET_H

#include "aux.h"
#include "Coordonnees.h"

/**
 * @class Objet
 * @brief Classe représentant un objet dans le jeu.
 */
class Objet {
protected:
    Coordonnees position; /**< Position de l'objet */
    int vitesse; /**< Vitesse de déplacement de l'objet */

public:
    /**
     * @brief Constructeur de la classe Objet.
     * @param x Position horizontale de l'objet.
     * @param y Position verticale de l'objet.
     * @param _vitesse Vitesse de déplacement de l'objet.
     */
    Objet(int x, int y, int _vitesse);

    /**
     * @brief Destructeur de la classe Objet.
     */
    ~Objet();

    // Méthodes d'accès
    /**
     * @brief Obtenir la position horizontale de l'objet.
     * @return La position horizontale de l'objet.
     */
    int getX() const;

    /**
     * @brief Obtenir la position verticale de l'objet.
     * @return La position verticale de l'objet.
     */
    int getY() const;

    // Méthodes de modification
    /**
     * @brief Définir la nouvelle position horizontale de l'objet.
     * @param x La nouvelle position horizontale de l'objet.
     */
    void setX(int x);

    /**
     * @brief Définir la nouvelle position verticale de l'objet.
     * @param y La nouvelle position verticale de l'objet.
     */
    void setY(int y);

    /**
     * @brief Déplacer l'objet dans une direction donnée.
     * @param x La direction horizontale de déplacement.
     * @param y La direction verticale de déplacement.
     */
    void move(int x, int y);

    /**
     * @brief Obtenir la vitesse de déplacement de l'objet.
     * @return La vitesse de déplacement de l'objet.
     */
    int getVitesse() const;
};

#endif // CODE_OBJET_H
