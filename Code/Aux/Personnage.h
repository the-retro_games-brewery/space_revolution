/**
 * @file Personnage.h
 * @brief Définition de la classe Personnage.
 */

#ifndef CODE_PERSONNAGE_H
#define CODE_PERSONNAGE_H

#include "aux.h"
#include "Bullet.h"
#include "Objet.h"
#include "Coordonnees.h"

/**
 * @class Personnage
 * @brief Classe représentant un personnage dans le jeu.
 */
class Personnage : public Objet {
private:
    int pointsDeVie; /**< Points de vie du personnage */
    std::vector<Bullet> balles; /**< Les balles tirées par le personnage */
    Direction direction; /**< Direction du personnage */

public:
    /**
     * @brief Constructeur de la classe Personnage.
     * @param _x Position horizontale du personnage.
     * @param _y Position verticale du personnage.
     * @param _pointsDeVie Points de vie du personnage.
     * @param _vitesse Vitesse de déplacement du personnage.
     */
    Personnage(int _x, int _y, int _pointsDeVie, int _vitesse);

    /**
     * @brief Destructeur de la classe Personnage.
     */
    ~Personnage();

    // Méthodes d'accès
    /**
     * @brief Obtenir les points de vie du personnage.
     * @return Les points de vie du personnage.
     */
    int getPointsDeVie() const;
    int get_vie() const;



    // Méthodes de modification
    /**
     * @brief Infliger des dégâts au personnage.
     * @param degat Les dégâts infligés au personnage.
     */
    void degat(int degat);

    /**
     * @brief Vérifier si le personnage est en vie.
     * @return Vrai si le personnage est en vie, sinon faux.
     */
    bool isAlive();

    /**
     * @brief Faire tourner le personnage.
     */
    void rotate();

    /**
     * @brief Tirer une balle à partir de la position actuelle du personnage.
     * @param x Position horizontale de la cible de la balle.
     * @param y Position verticale de la cible de la balle.
     * @param vitesse Vitesse de déplacement de la balle.
     */
    void tirerBalle(int x, int y, int vitesse);

    /**
     * @brief Obtenir les balles tirées par le personnage.
     * @return Une référence constante vers le vecteur de balles.
     */
    const std::vector<Bullet>& getBalles() const;

    /**
     * @brief Mettre à jour les balles tirées par le personnage.
     * @param x Nouvelle position horizontale.
     * @param y Nouvelle position verticale.
     */
    void updateBalles(int x, int y);

    /**
     * @brief Mettre à jour les balles tirées par le personnage une à une.
     * @param x Nouvelle position horizontale.
     * @param y Nouvelle position verticale.
     */
    void updateBallesDistance(int x, int y);

    /**
     * @brief Obtenir la direction du personnage.
     * @return La direction du personnage.
     */
    Direction getDirection();

    /**
     * @brief Modifier la direction du personnage.
     * @param direction La nouvelle direction du personnage.
     */
    void setDirection(Direction direction);

    /**
     * @brief Supprimer une balle du vecteur de balles.
     * @param balle La balle à supprimer.
     */
    void supprimerBalle(const Bullet& balle);

    /**
     * @brief Obtenir la vitesse du personnage.
     * @return La vitesse du personnage.
     */
    int getVitesse() const;

    /**
     * @brief Modifier la vitesse du personnage.
     * @param Vit La nouvelle vitesse du personnage.
     */
    void setVitesse(int Vit);

    /**
     * @brief Obtenir la position horizontale du personnage.
     * @return La position horizontale du personnage.
     */
    int getX() const;

    /**
     * @brief Obtenir la position verticale du personnage.
     * @return La position verticale du personnage.
     */
    int getY() const;

    /**
     * @brief Surcharge de l'opérateur d'égalité.
     * @param autre_per L'objet Personnage à comparer.
     * @return true si les personnages sont identiques, sinon false.
     */
    bool operator==(const Personnage& autre_per) const;

    /**
     * @brief Vider le vecteur de balles.
     */
    void viderBalles();
};

#endif // CODE_PERSONNAGE_H
