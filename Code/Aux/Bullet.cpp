#include "Bullet.h"

// Implémentation de la classe Bullet
// Dans la classe Bullet
Bullet::Bullet(int x, int y, int _puissance, std::string _type, int _vitesse)
        : Objet(x, y, _vitesse), puissance(_puissance), type(_type) {}


Bullet::~Bullet() {}


int Bullet::getPuissance() const { return puissance; }
std::string Bullet::getType() const { return type; }
void Bullet::setPuissance(int _puissance) { puissance = _puissance; }
void Bullet::setType(std::string _type) { type = _type; }
void Bullet::move(int x,int y) {
    Objet::move(x,y);
}

void deplacerAB(int x, int y) {
}

void Bullet::collision() const {
    // Implémentez la logique de collision ici
}
