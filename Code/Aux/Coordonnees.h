//
// Created by Mac on 03/04/2024.
//

#ifndef CODE_COORDONNEES_H
#define CODE_COORDONNEES_H

#include "aux.h"



/**
 * @brief Classe représentant les coordonnées d'un objet dans un système de coordonnées (x, y).
 */
class Coordonnees {
private:
    int x; /**< Position horizontale */
    int y; /**< Position verticale */

public:
    /**
     * @brief Constructeur de la classe Coordonnees.
     * @param _x Position horizontale.
     * @param _y Position verticale.
     */
    Coordonnees(int _x, int _y);

    // Méthodes d'accès
    int getX() const; /**< @return La position horizontale */
    int getY() const; /**< @return La position verticale */

    // Méthodes de modification
    void setX(int _x); /**< @param _x La nouvelle position horizontale */
    void setY(int _y); /**< @param _y La nouvelle position verticale */

    // Méthode pour afficher les coordonnées
    void afficher() const;

    friend std::ostream& operator<<(std::ostream& os, const Coordonnees& coord);

};

#endif //CODE_COORDONNEES_H
