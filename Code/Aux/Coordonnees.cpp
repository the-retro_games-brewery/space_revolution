#include "Coordonnees.h"

// Implémentation de la classe Coordonnees
Coordonnees::Coordonnees(int _x, int _y) : x(_x), y(_y) {}

int Coordonnees::getX() const { return x; }
int Coordonnees::getY() const { return y; }
void Coordonnees::setX(int _x) { x = _x; }
void Coordonnees::setY(int _y) { y = _y; }
void Coordonnees::afficher() const {
    std::cout << "(" << x << ", " << y << ")" << std::endl;
}
