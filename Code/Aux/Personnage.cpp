#include "Personnage.h"

Personnage::Personnage(int x, int y, int _pointsDeVie, int _vitesse)
        : Objet(x, y, _vitesse), pointsDeVie(_pointsDeVie) {}

Personnage::~Personnage() {}

int Personnage::getPointsDeVie() const { return pointsDeVie; }
void Personnage::degat(int degat) { pointsDeVie -= degat; }
bool Personnage::isAlive() { return pointsDeVie > 0; }
void Personnage::tirerBalle(int x, int y, int vitesse) {
    balles.push_back(Bullet(x, y, 1, "balle", vitesse));
}
const std::vector<Bullet>& Personnage::getBalles() const { return balles; }

void Personnage::updateBalles(int x, int y) {
    for (auto& balle : balles) {
        // Supposons que les balles se déplacent vers la droite
        balle.move(x,y);
    }
}

void Personnage::updateBallesDistance(int x, int y) {
    // Déplace les balles de 1 en direction des coordonnées x et y
    for (auto& balle : balles) {
        // Calculer le déplacement horizontal et vertical nécessaire pour atteindre les coordonnées (x, y)
        int dx = (x > balle.getX()) ? 1 : ((x < balle.getX()) ? -1 : 0);
        int dy = (y > balle.getY()) ? 1 : ((y < balle.getY()) ? -1 : 0);
        
        // Déplacer la balle d'un pas dans la direction calculée
        balle.move(dx, dy);
    }
}

void Personnage::rotate() {
    // Implémentez la logique de rotation ici
}

Direction Personnage::getDirection() {
    return direction;
}

void Personnage::setDirection(Direction direction) {
    this->direction = direction;
}

void Personnage::supprimerBalle(const Bullet& balle) {
    // Recherche de la balle à supprimer dans le vecteur balles
    auto it = std::find(balles.begin(), balles.end(), balle);
    // Si la balle est trouvée, l'effacer du vecteur
    if (it != balles.end()) {
        balles.erase(it);
    }
}

int Personnage::getVitesse() const {
    return vitesse;
}

void Personnage::setVitesse(int Vit) {
    vitesse = Vit;
}

int Personnage::getX() const {
    return Objet::getX();
}

int Personnage::getY() const {
    return Objet::getY();
}

int Personnage::get_vie() const {
    return pointsDeVie;
}

bool Personnage::operator == (const Personnage& autre_per) const {
    return (getX() == autre_per.getX()) && (getY() == autre_per.getY());}

void Personnage::viderBalles() { balles.clear(); }