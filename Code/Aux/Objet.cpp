#include "Objet.h"

// Implémentation de la classe Objet
Objet::Objet(int x, int y, int _vitesse) : position(x, y), vitesse(_vitesse) {}
Objet::~Objet() {}

int Objet::getX() const { return position.getX(); }
int Objet::getY() const { return position.getY(); }
void Objet::setX(int x) { position.setX(x); }
void Objet::setY(int y) { position.setY(y); }
int Objet::getVitesse() const { return vitesse; }
void Objet::move(int x,int y) {
    // Déplacer l'objet dans une direction donnée en fonction de ca vitesse
    position.setX(position.getX() + x * vitesse);
    position.setY(position.getY() + y * vitesse);

}