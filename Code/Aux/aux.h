#ifndef CODE_AUX_H
#define CODE_AUX_H

#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <cstdlib>
#include <ctime>
#include <chrono>
#include <thread>
#include <fstream>

#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"
#include "../Functions/Fonctions.h"

enum Direction {haut, bas, gauche, droite};

#endif //CODE_AUX_H
