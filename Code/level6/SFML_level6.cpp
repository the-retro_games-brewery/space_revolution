#include "SFML_level6.h"

SFML_level6::SFML_level6(sf::RenderWindow& window)
        : window(window), niveau() {
    TextureLoader();
    selected = 1;
}


void SFML_level6::TextureLoader() {
    if (!playerTexture.loadFromFile("../assets/character/Player/1/eA7sqMSeSc2H07LxGAmZMQ_background-removal_1706720074 copie 4.png")) {
        std::cerr << "Erreur lors du chargement de la texture du joueur" << std::endl;
    }
    if (!carte11.loadFromFile("../assets/Planets/ground/carte.png")) {
        std::cerr << "Erreur lors du chargement de la texture de carte11" << std::endl;}

    if (!screenTexture.loadFromFile("../assets/Backgrounds/Game/Hacking/screen.png")) {
        std::cerr << "Erreur lors du chargement de la texture de l'écran" << std::endl;
    }
        if (!font.loadFromFile("../assets/GUI/fonts/main.otf")) {
        std::cerr << "Erreur lors du chargement de la police de caractères" << std::endl;
    }

    if (!tourret1Texture.loadFromFile("../assets/Building/Turret1.png")) {
        std::cerr << "Erreur lors du chargement de la texture de tourret1" << std::endl;
    }
    if (!tourret2Texture.loadFromFile("../assets/Building/Turret2.png")) {
        std::cerr << "Erreur lors du chargement de la texture de tourret2" << std::endl;
    }
    if (!tourret3Texture.loadFromFile("../assets/Building/Turret3.png")) {
        std::cerr << "Erreur lors du chargement de la texture de tourret3" << std::endl;
    }
    if (!tourret4Texture.loadFromFile("../assets/Building/Turret4.png")) {
        std::cerr << "Erreur lors du chargement de la texture de tourret4" << std::endl;
    }
    if (!tourret5Texture.loadFromFile("../assets/Building/Turret5.png")) {
        std::cerr << "Erreur lors du chargement de la texture de tourret5" << std::endl;
    }
    if (!tourret6Texture.loadFromFile("../assets/Building/Turret6.png")) {
        std::cerr << "Erreur lors du chargement de la texture de tourret6" << std::endl;
    }
}


void SFML_level6::afficherInventaire() {
    // Définir la position de départ de la matrice d'inventaire
    int startX = 50;
    int startY = window.getSize().y - 300; // Position verticale dans le coin inférieur gauche
    int spacingX = 150; // Espacement horizontal entre les images (augmenté)
    int spacingY = 150; // Espacement vertical entre les images (augmenté)
    
    // Dessiner le fond semi-transparent avec une teinte verte
    sf::RectangleShape background(sf::Vector2f(3 * spacingX + 20, 2 * spacingY + 20)); // Taille du fond basée sur le nombre de colonnes et de lignes
    background.setFillColor(sf::Color(0, 255, 0, 80)); // Teinte verte semi-transparente
    background.setPosition(startX - 20, startY - 20); // Positionnement dans le coin inférieur gauche
    background.setScale(1, 1); // Agrandir le fond
    window.draw(background);
    
    // Afficher les images des tourelles dans une matrice
    for (int i = 0; i < 6; ++i) {
        sf::Sprite turretSprite;
        switch (i) {
            case 0:
                turretSprite.setTexture(tourret1Texture);
                break;
            case 1:
                turretSprite.setTexture(tourret2Texture);
                break;
            case 2:
                turretSprite.setTexture(tourret3Texture);
                break;
            case 3:
                turretSprite.setTexture(tourret4Texture);
                break;
            case 4:
                turretSprite.setTexture(tourret5Texture);
                break;
            case 5:
                turretSprite.setTexture(tourret6Texture);
                break;
            default:
                break;
        }
        
        turretSprite.setPosition(startX + (i % 3) * spacingX, startY + (i / 3) * spacingY);
        //affiche turetSprite par rapport a la taille de la fenetre
        turretSprite.setScale(0.75, 0.75);
        window.draw(turretSprite);
    }
    
    // Gérer la sélection de la tourelle par le joueur
    int selectedRow = selected / 3;
    int selectedCol = selected % 3;
    sf::RectangleShape selectionRect(sf::Vector2f(130, 130)); // Rectangle de sélection agrandi
    selectionRect.setFillColor(sf::Color::Transparent);
    selectionRect.setOutlineThickness(2);
    selectionRect.setOutlineColor(sf::Color::Green);
    selectionRect.setPosition(startX + selectedCol * spacingX - 20, startY + selectedRow * spacingY - 20); // Positionnement ajusté pour l'agrandissement
    selectionRect.setScale(1, 1); // Agrandir le rectangle de sélection
    window.draw(selectionRect);
}



void SFML_level6::Render(int mouseX, int mouseY) {
int tailleTuileX = window.getSize().x / niveau.getTailleMapX();
int tailleTuileY = window.getSize().y / niveau.getTailleMapY();

    window.clear();



    sf::Sprite backgroundSprite(carte11);
    backgroundSprite.setScale(window.getSize().x / backgroundSprite.getLocalBounds().width, window.getSize().y / backgroundSprite.getLocalBounds().height);
    window.draw(backgroundSprite);
    // Dessin des ennemis
    for (auto& ennemi : niveau.getEnnemis()) {
        if (ennemi.getPointsDeVie() > 0 && ennemi.getX() > 0 && ennemi.getY() > 0) {
            sf::CircleShape enemyCircle(0.5 * std::min(tailleTuileX, tailleTuileY)); // Création du cercle
            enemyCircle.setPosition(ennemi.getX() * tailleTuileX, ennemi.getY() * tailleTuileY); // Positionnement
            
            // Mesurer le temps écoulé pour le clignotement
            sf::Time elapsed = blinkClock.getElapsedTime();
            float blinkDuration = 0.0; // Durée de chaque clignotement en secondes
            bool isVisible = (static_cast<int>(elapsed.asSeconds() / blinkDuration) % 2 == 0);

            if (isVisible) {
                enemyCircle.setFillColor(sf::Color::Green);
                window.draw(enemyCircle);
            } else {
                enemyCircle.setFillColor(sf::Color::Transparent);
                window.draw(enemyCircle);
            }
        }
    }

        for (auto& tourelle : niveau.getTourelles()) {
        sf::Sprite tourelleSprite;
        // Définir la texture de la tourelle en fonction de sa puissance
        switch (tourelle.getVitesse()) {
            case 1:
                tourelleSprite.setTexture(tourret1Texture);
                break;
            case 2:
                tourelleSprite.setTexture(tourret2Texture);
                break;
            case 3:
                tourelleSprite.setTexture(tourret3Texture);
                break;
            case 4:
                tourelleSprite.setTexture(tourret4Texture);
                break;
            case 5:
                tourelleSprite.setTexture(tourret5Texture);
                break;
            case 6:
                tourelleSprite.setTexture(tourret6Texture);
                break;
            default:
                break;
        }
        // Calculer les coordonnées de la tourelle sur l'écran en fonction de ses coordonnées sur la carte
// Calculer les coordonnées de la tourelle sur l'écran en fonction de ses coordonnées sur la carte
// Calculer les coordonnées de la tourelle sur l'écran en fonction de ses coordonnées sur la carte
int posX = tourelle.getX() * tailleTuileX - tourelleSprite.getGlobalBounds().width / 2;
int posY = tourelle.getY() * tailleTuileY - tourelleSprite.getGlobalBounds().height / 2;


        tourelleSprite.setPosition(posX, posY);
        tourelleSprite.setScale(0.5, 0.5);
        window.draw(tourelleSprite);
    }

    

    // Ajout du filtre vert
    sf::RectangleShape greenFilter(sf::Vector2f(window.getSize().x, window.getSize().y));
    greenFilter.setFillColor(sf::Color(0, 255, 0, 60)); // Vert semi-transparent
    window.draw(greenFilter);

    // Create a rectangle shape for the green wave
    sf::RectangleShape greenWave(sf::Vector2f(200, window.getSize().y));
    greenWave.setFillColor(sf::Color(0, 255, 0, 80));
    
    // Set the initial position of the green wave
    static float waveX = 0; // Make it static to persist its value across function calls
    float waveY = 0; // Set the desired Y position of the green wave
    
    // Get the time elapsed since the last frame and convert it to seconds
    float deltaTime = renderClock.restart().asSeconds(); // <-- Utilisez ici votre nouvelle clock
    
    // Update the position of the green wave based on the time elapsed
    float waveSpeed = 600.0f; // Adjust the speed of the wave
    
    waveX += waveSpeed * deltaTime; 

    // Wrap the wave back to the left side of the window when it reaches the right side
    if (waveX > window.getSize().x) {
        waveX = 0;
    }

    // Set the position of the green wave
    greenWave.setPosition(waveX, waveY);
    // Draw the green wave
    window.draw(greenWave);

    // Affichage des coordonnées de la souris dans le coin supérieur droit de l'écran
    sf::Text mouseCoordinates;
    mouseCoordinates.setFont(font); // Définir la police pour le texte
    mouseCoordinates.setCharacterSize(20); // Définir la taille des caractères
    mouseCoordinates.setFillColor(sf::Color::Green); // Définir la couleur du texte
    mouseCoordinates.setPosition(window.getSize().x - mouseCoordinates.getLocalBounds().width - 200, 60); // Définir la position du texte
    mouseCoordinates.setString("[" + std::to_string(mouseX) + " _ " + std::to_string(mouseY) + "]"); // Définir le contenu du texte avec les coordonnées de la souris
    window.draw(mouseCoordinates); // Dessiner le texte

    // Dessin du viseur de la souris
    sf::RectangleShape crosshair(sf::Vector2f(40, 40));
    crosshair.setFillColor(sf::Color::Transparent);
    crosshair.setOutlineThickness(2);
    crosshair.setOutlineColor(sf::Color::Green);
    crosshair.setPosition(mouseX - 20, mouseY - 20);

    sf::RectangleShape horizontalLine(sf::Vector2f(4000, 2));
    horizontalLine.setFillColor(sf::Color::Green);
    horizontalLine.setPosition(0, mouseY);

    sf::RectangleShape verticalLine(sf::Vector2f(2, 4000));
    verticalLine.setFillColor(sf::Color::Green);
    verticalLine.setPosition(mouseX, 0);

    window.draw(crosshair);
    window.draw(horizontalLine);
    window.draw(verticalLine);
    
    sf::Text message;
    message.setFont(font); // Set the font for the text
    message.setString("KiloBit Solutions facilities \n -------------------------\nSAT_View 1: \nScanning mode [On]\nNightVision mode [On] \nDetecting many Targets : " + std::to_string(niveau.getNbEnnemis()) + "\n Defense system status [Ready] \n Turret command mode [Auto]"); // Set the text content
    message.setCharacterSize(15); // Set the character size
    message.setFillColor(sf::Color::Green); // Set the text color
    message.setPosition(window.getSize().x - message.getLocalBounds().width - 20, window.getSize().y - message.getLocalBounds().height - 20); // Set the position of the text
    window.draw(message); // Draw the text

    // Affichage des nombres aléatoires à l'écran
    sf::Text randomNumbers;
    randomNumbers.setFont(font); // Définir la police pour le texte
    randomNumbers.setCharacterSize(12); // Définir la taille des caractères
    randomNumbers.setFillColor(sf::Color::Green); // Définir la couleur du texte
    randomNumbers.setPosition(20, window.getSize().x/3); // Définir la position du texte
    int randomNumber = rand() % 999999999;


    // Convertir le nombre en chaîne de caractères
    std::string randomNumberString = std::to_string(randomNumber);
    std::string randomNumberString1 = std::to_string(randomNumber*2);
    std::string randomNumberString2 = std::to_string(randomNumber*3);
    std::string randomNumberString3 = std::to_string(randomNumber*4);
    std::string randomNumberString4 = std::to_string(randomNumber*5);
    std::string randomNumberString5 = std::to_string(randomNumber*6);
    std::string randomNumberString6 = std::to_string(randomNumber*7);
    std::string randomNumberString7 = std::to_string(randomNumber*8);
    std::string randomNumberString8 = std::to_string(randomNumber*9);
    std::string randomNumberString9 = std::to_string(randomNumber*10);
    std::string randomNumberString10 = std::to_string(randomNumber*11);
    std::string randomNumberString11 = std::to_string(randomNumber*12);
    std::string randomNumberString12 = std::to_string(randomNumber*13);
    std::string randomNumberString13 = std::to_string(randomNumber*14);
    std::string randomNumberString14 = std::to_string(randomNumber*15);
    std::string randomNumberString15 = std::to_string(randomNumber*16);
    std::string randomNumberString16 = std::to_string(randomNumber*17);
    std::string randomNumberString17 = std::to_string(randomNumber*18);
    std::string randomNumberString18 = std::to_string(randomNumber*19);
    std::string randomNumberString19 = std::to_string(randomNumber*20);
    std::string randomNumberString20 = std::to_string(randomNumber*21);
    std::string randomNumberString21 = std::to_string(randomNumber*22);

    // Définir le contenu du texte avec le nombre aléatoire
    randomNumbers.setString("Coord calculation output : \n"+randomNumberString1+"-"+randomNumberString2+"-"+
    randomNumberString3+"\n"+randomNumberString4+"-"+randomNumberString5+"-"+randomNumberString6+"\n"+randomNumberString7+"-"+
    randomNumberString8+"-"+randomNumberString9+"\n"+randomNumberString10+"-"+randomNumberString11+"-"+randomNumberString12+"\n"+
    randomNumberString13+"-"+randomNumberString14+"-"+randomNumberString15+"\n"+randomNumberString16+"-"+randomNumberString17+"-"+
    randomNumberString18+"\n"+randomNumberString19+"-"+randomNumberString20+"-"+randomNumberString21);
    randomNumbers.setPosition(10,100);
    window.draw(randomNumbers); // Dessiner le texte
    // Affichage du compteur de monnaie
    sf::Text coinCounter;
    coinCounter.setFont(font); // Définir la police pour le texte
    coinCounter.setString("Energy : " + std::to_string(niveau.getCoin())); // Définir le contenu du texte avec le compteur de monnaie
    coinCounter.setCharacterSize(30); // Définir la taille des caractères
    coinCounter.setFillColor(sf::Color::Green); // Définir la couleur du texte
    coinCounter.setPosition(20, 20); // Définir la position du texte
    window.draw(coinCounter); // Dessiner le texte
    afficherInventaire();
/*
    sf::Sprite screenSprite(screenTexture);
    screenSprite.setScale(window.getSize().x / screenSprite.getLocalBounds().width, window.getSize().y / screenSprite.getLocalBounds().height);
    window.draw(screenSprite);
    // Apply waves filter to the screen
    */
    window.display();
}





int SFML_level6::run() {

    int mouseX;
    int mouseY;
    sf::Clock clock; // Créez une horloge pour mesurer le temps écoulé
    float tempsEntreMisesAJour = 0.3f; // Temps entre les mises à jour en secondes
    float tempsdebutJeu = 2.0f; // Temps avant le début du jeu en secondes
    TextureLoader();
    bool debutJeu = false;
    sf::Music music;
    niveau.recupMap();

    sf::String musicFile= "../Sounds/inGame/Back in Black.mp3";
    music.openFromFile(musicFile);
    music.play();
    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
            }

            if (event.type == sf::Event::MouseMoved) {
                mouseX = event.mouseMove.x;
                mouseY = event.mouseMove.y;
            }

            if (event.type == sf::Event::MouseButtonPressed) {
                std::cout << "Mouse button pressed" << std::endl;
                std::cout << "Mouse X: " << mouseX << " Mouse Y: " << mouseY << std::endl;
                if (event.mouseButton.button == sf::Mouse::Left) {
                    if (mouseX >= 332 && mouseX <= 360 && mouseY >= 88 && mouseY <= 117) {
                        if(selected == 0){
                            if (niveau.getCoin() >= 1){
                                niveau.creerTourlevel(18, 7, 1);
                            }
                        }
                        if(selected == 1){
                            if (niveau.getCoin() >= 2){
                                niveau.creerTourlevel(18, 7, 2);
                            }
                        }
                        if(selected == 2){
                            if (niveau.getCoin() >= 3){
                                niveau.creerTourlevel(18, 7, 3);
                            }
                        }
                        if(selected == 3){
                            if (niveau.getCoin() >= 4){
                                niveau.creerTourlevel(18, 7, 4);
                            }
                        }
                        if(selected == 4){
                            if (niveau.getCoin() >= 5){
                                niveau.creerTourlevel(17, 8, 5);
                            }
                        }
                        if(selected == 5){
                            if (niveau.getCoin() >= 6){
                                niveau.creerTourlevel(17, 8, 6);
                            }
                        }
                    }
                    if (mouseX >= 307 && mouseX <= 336 && mouseY >= 296 && mouseY <= 329) {
                        if(selected == 0){
                            if (niveau.getCoin() >= 1){
                                niveau.creerTourlevel(17, 17, 1);
                            }
                        }
                        if(selected == 1){
                            if (niveau.getCoin() >= 2){
                                niveau.creerTourlevel(17, 17, 2);
                            }
                        }
                        if(selected == 2){
                            if (niveau.getCoin() >= 3){
                                niveau.creerTourlevel(17, 17, 3);
                            }
                        }
                        if(selected == 3){
                            if (niveau.getCoin() >= 4){
                                niveau.creerTourlevel(17, 17, 4);
                            }
                        }
                        if(selected == 4){
                            if (niveau.getCoin() >= 5){
                                niveau.creerTourlevel(17, 17, 5);
                            }
                        }
                        if(selected == 5){
                            if (niveau.getCoin() >= 6){
                                niveau.creerTourlevel(17, 17, 6);
                            }
                        }
                    }
                    if (mouseX >= 783 && mouseX <= 812 && mouseY >= 155 && mouseY <= 184) {
                        if(selected == 0){
                            if (niveau.getCoin() >= 1){
                                niveau.creerTourlevel(41, 9, 1);
                            }
                        }
                        if(selected == 1){
                            if (niveau.getCoin() >= 2){
                                niveau.creerTourlevel(41, 9, 2);
                            }
                        }
                        if(selected == 2){
                            if (niveau.getCoin() >= 3){
                                niveau.creerTourlevel(41, 9, 3);
                            }
                        }
                        if(selected == 3){
                            if (niveau.getCoin() >= 4){
                                niveau.creerTourlevel(41, 9, 4);
                            }
                        }
                        if(selected == 4){
                            if (niveau.getCoin() >= 5){
                                niveau.creerTourlevel(41, 9, 5);
                            }
                        }
                        if(selected == 5){
                            if (niveau.getCoin() >= 6){
                                niveau.creerTourlevel(41, 9, 6);
                            }
                        }
                    }
                    if (mouseX >= 244 && mouseX <= 272 && mouseY >= 428 && mouseY <= 460) {
                        if(selected == 0){
                            if (niveau.getCoin() >= 1){
                                niveau.creerTourlevel(14, 23, 1);
                            }
                        }
                        if(selected == 1){
                            if (niveau.getCoin() >= 2){
                                niveau.creerTourlevel(14, 23, 2);
                            }
                        }
                        if(selected == 2){
                            if (niveau.getCoin() >= 3){
                                niveau.creerTourlevel(14, 23, 3);
                            }
                        }
                        if(selected == 3){
                            if (niveau.getCoin() >= 4){
                                niveau.creerTourlevel(14, 23, 4);
                            }
                        }
                        if(selected == 4){
                            if (niveau.getCoin() >= 5){
                                niveau.creerTourlevel(14, 23, 5);
                            }
                        }
                        if(selected == 5){
                            if (niveau.getCoin() >= 6){
                                niveau.creerTourlevel(14, 23, 6);
                            }
                        }
                    }
                    if (mouseX >= 913 && mouseX <= 940 && mouseY >= 371 && mouseY <= 401) {
                        if(selected == 0){
                            if (niveau.getCoin() >= 1){
                                niveau.creerTourlevel(47, 20, 1);
                            }
                        }
                        if(selected == 1){
                            if (niveau.getCoin() >= 2){
                                niveau.creerTourlevel(47, 20, 2);
                            }
                        }
                        if(selected == 2){
                            if (niveau.getCoin() >= 3){
                                niveau.creerTourlevel(47, 20, 3);
                            }
                        }
                        if(selected == 3){
                            if (niveau.getCoin() >= 4){
                                niveau.creerTourlevel(47, 20, 4);
                            }
                        }
                        if(selected == 4){
                            if (niveau.getCoin() >= 5){
                                niveau.creerTourlevel(47, 20, 5);
                            }
                        }
                        if(selected == 5){
                            if (niveau.getCoin() >= 6){
                                niveau.creerTourlevel(47, 20, 6);
                            }
                        }
                    }
                    if (mouseX >= 871 && mouseX <= 898 && mouseY >= 669 && mouseY <= 698) {
                        if(selected == 0){
                            if (niveau.getCoin() >= 1){
                                niveau.creerTourlevel(45, 35, 1);
                            }
                        }
                        if(selected == 1){
                            if (niveau.getCoin() >= 2){
                                niveau.creerTourlevel(45, 35, 2);
                            }
                        }
                        if(selected == 2){
                            if (niveau.getCoin() >= 3){
                                niveau.creerTourlevel(45, 35, 3);
                            }
                        }
                        if(selected == 3){
                            if (niveau.getCoin() >= 4){
                                niveau.creerTourlevel(45, 35, 4);
                            }
                        }
                        if(selected == 4){
                            if (niveau.getCoin() >= 5){
                                niveau.creerTourlevel(45, 35, 5);
                            }
                        }
                        if(selected == 5){
                            if (niveau.getCoin() >= 6){
                                niveau.creerTourlevel(45, 35, 6);
                            }
                        }
                    }
                }
            }
            // Gérer les entrées clavier pour la sélection de la tourelle
            if (event.type == sf::Event::KeyPressed) {
                if (event.key.code == sf::Keyboard::Q) {
                    if (selected % 3 > 0) {
                        selected--;
                        std::cout << selected << std::endl;
                    }
                }
                else if (event.key.code == sf::Keyboard::D) {
                    if (selected % 3 < 2) {
                        selected++;
                        std::cout << selected << std::endl;
                    }
                }
                else if (event.key.code == sf::Keyboard::Z) {
                    if (selected >= 3) {
                        selected -= 3;
                        std::cout << selected << std::endl;
                    }
                }
                else if (event.key.code == sf::Keyboard::S) {
                    if (selected < 3) {
                        selected += 3;
                        std::cout << selected << std::endl;
                    }
                }
                else if (event.key.code == sf::Keyboard::Escape) {
                    music.stop();
                    gamePause(window);
                    music.play();
                }
            }
        }
        Render(mouseX, mouseY);
        std::cout << "Temps écoulé : " << clock.getElapsedTime().asSeconds() << std::endl;
        if (clock.getElapsedTime().asSeconds() >= tempsdebutJeu){
            debutJeu = true;
        }
        if (debutJeu == true) {
        if (clock.getElapsedTime().asSeconds() >= tempsEntreMisesAJour) {
            int gameState = 0;
                gameState = niveau.mettreAJour();

                if (gameState == 2) {
                    std::cout << "Vous avez gagné !" << std::endl;
                    return 2;
                } else if (gameState == 1) {
                    std::cout << "Vous avez perdu !" << std::endl;
                    return 1;
                }
                
            Render(mouseX, mouseY);
            clock.restart();
        }
        }
    }
    music.stop();
    
    window.close();
    return 0;
}



/*
int main(){
    sf::RenderWindow window(sf::VideoMode(1200, 840), "SFML works!");
    SFML_level6 niveau(window);
    niveau.run();
    return 0;
}
*/


//g++ -std=c++17 SFML_level6.cpp level6.cpp ../Aux/Personnage.cpp ../Aux/Objet.cpp ../Aux/Coordonnees.cpp ../Aux/Bullet.cpp ../Functions/pause.cpp -o testGLV6 -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio