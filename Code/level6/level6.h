// Level6.h
#ifndef CODE_LEVEL6_H
#define CODE_LEVEL6_H

#include "../Aux/aux.h"
#include "../Aux/Objet.h"
#include "../Aux/Personnage.h"
#include "../Aux/Coordonnees.h"
#include <vector>
#include <queue>
#include <unordered_map>
#include <fstream>
#include <ctime>


struct Noeud { // une struct qui va nous servir pour implémenter l'algorithme plus court chemin
    int x, y;
    float cout;
    float heuristique;
    Noeud* parent;

    Noeud(int x, int y, Noeud* parent = nullptr);
    float scoreTotal() const;   
};

struct CompareNoeud {
    bool operator()(const Noeud* a, const Noeud* b) const;
};


class Niveau6 {
private:
    int tailleMapX = 60;
    int tailleMapY = 40;
    char entree;
    int nb = 5;
    int TotalEnnemis = 0;
    int compteur = 0;
    int jetons = 4;
    Coordonnees obj = Coordonnees(49, 21); // Définir l'objectif des ennemis
    std::vector<Objet> Obsts;
    std::vector<Objet> Vides;
    std::vector<Personnage> Tourelles;
    std::vector<Personnage> Ennemis;  
    std::vector<Coordonnees> EmpEnnemis;   
    std::vector<Coordonnees> EmpTours;   

public:
    Niveau6();
    ~Niveau6();
    void afficher();
    void recupMap();
    int getTailleMapX() const;
    int getTailleMapY() const;
    const std::vector<Personnage>& getEnnemis() const;
    const std::vector<Personnage>& getTourelles() const;
    int getCoin() const;
    int getNbEnnemis() const;

    bool estObst(int x, int y);
    bool estVide(int x, int y);
    
    void creerObst(int x,int y);
    void creerVide(int x,int y);
    void creerTourelle(int x,int y);
    void creerEnnemi(int x,int y);
    
    void miniIA();
    bool deplacerEnnemi(int x,int y);
    int heuristique(int x1, int y1, int x2, int y2);
    std::vector<Noeud*> calculerPlusCourtChemin(int departX, int departY, int cibleX, int cibleY);

    int mettreAJour();
    void creerTourlevel(int x,int y, int p);    

    void viderTourelles();
    void viderEnnemis();
    void viderObsts();
    void viderVides();
    void viderEmpEnnemis();
    void viderEmpTours();
};

#endif //CODE_LEVEL6_H