#ifndef CODE_SFML_LEVEL6_H
#define CODE_SFML_LEVEL6_H

#include "level6.h"

/**
 * @class SFML_level6
 * @brief Classe gérant l'affichage et le fonctionnement du sixième niveau du jeu en utilisant la bibliothèque SFML.
 */
class SFML_level6 {
private:
    Niveau6 niveau; /**< Instance du sixième niveau du jeu */
    sf::RenderWindow& window; /**< Référence vers la fenêtre SFML */

    // Textures des éléments du jeu
    sf::Texture playerTexture; /**< Texture du joueur */
    sf::Texture carte11; /**< Texture de la première carte */
    sf::Texture tourret1Texture; /**< Texture de la tourrette 1 */
    sf::Texture tourret2Texture; /**< Texture de la tourrette 2 */
    sf::Texture tourret3Texture; /**< Texture de la tourrette 3 */
    sf::Texture tourret4Texture; /**< Texture de la tourrette 4 */
    sf::Texture tourret5Texture; /**< Texture de la tourrette 5 */
    sf::Texture tourret6Texture; /**< Texture de la tourrette 6 */
    sf::Texture screenTexture; /**< Texture de l'écran */
    sf::Clock renderClock; /**< Horloge pour l'affichage */
    sf::Clock blinkClock; /**< Horloge pour le clignotement */
    sf::Clock debutclock; /**< Horloge pour le début */
    sf::Font font; /**< Police de caractères */
    int selected; /**< Sélectionné */

public:
    /**
     * @brief Constructeur de la classe SFML_level6.
     * @param window La fenêtre SFML où afficher le niveau.
     */
    SFML_level6(sf::RenderWindow& window);

    /**
     * @brief Affiche l'inventaire du joueur.
     */
    void afficherInventaire();

    /**
     * @brief Charge les textures nécessaires pour le niveau.
     */
    void TextureLoader();

    /**
     * @brief Affiche le niveau dans la fenêtre SFML.
     * @param mouseX La position horizontale de la souris.
     * @param mouseY La position verticale de la souris.
     */
    void Render(int mouseX, int mouseY);

    /**
     * @brief Lance le sixième niveau du jeu.
     * @return Le résultat du jeu.
     */
    int run(); // Run the game

    /**
     * @brief Exécute le sixième niveau du jeu.
     * @return Le résultat du jeu.
     */
    int niveau6();
};

#endif // CODE_SFML_LEVEL6_H
