// Level6.cpp
#include "level6.h"  // Inclut le fichier d'en-tête associé à ce fichier source.

// Définition du constructeur pour la classe Noeud. Initialise les coordonnées x, y, le coût, l'heuristique, et le parent du noeud.
Noeud::Noeud(int x, int y, Noeud* parent)
    : x(x), y(y), cout(0), heuristique(0), parent(parent) {}

// Méthode qui calcule et retourne le score total du noeud en ajoutant le coût et l'heuristique.
float Noeud::scoreTotal() const {
    return cout + heuristique;
}
const std::vector<Personnage>& Niveau6::getEnnemis() const {
    return Ennemis;
}

const std::vector<Personnage>& Niveau6::getTourelles() const {
    return Tourelles;
}

int Niveau6::getCoin() const {
    return jetons;
}

int Niveau6::getNbEnnemis() const {
    return Ennemis.size();
}

// Opérateur de comparaison pour comparer deux noeuds en fonction de leur score total.
bool CompareNoeud::operator()(const Noeud* a, const Noeud* b) const {
    return a->scoreTotal() > b->scoreTotal();
}

// Constructeur de la classe Niveau6. Initialise la variable membre dernierEnnemi avec l'heure actuelle.
Niveau6::Niveau6() {
    srand(time(0));
}

// Destructeur vide pour la classe Niveau6.
Niveau6::~Niveau6() {
    viderTourelles();
    viderEnnemis();
    viderObsts();
    viderVides();
    viderEmpEnnemis();
    viderEmpTours();
}

// Méthode pour vider le vecteur de tourelles.
void Niveau6::viderTourelles() {
    Tourelles.clear();
}

// Méthode pour vider le vecteur d'ennemis.
void Niveau6::viderEnnemis() {
    Ennemis.clear();
}

// Méthode pour vider le vecteur d'obstacles.
void Niveau6::viderObsts() {
    Obsts.clear();
}

// Méthode pour vider le vecteur de cases vides.
void Niveau6::viderVides() {
    Vides.clear();
}

// Méthode pour vider le vecteur d'emplacements possibles pour les ennemis.
void Niveau6::viderEmpEnnemis() {
    EmpEnnemis.clear();
}

// Méthode pour vider le vecteur d'emplacements possibles pour les tourelles.
void Niveau6::viderEmpTours() {
    EmpTours.clear();
}

// Méthode pour charger une carte de jeu à partir d'un fichier.
void Niveau6::recupMap() {
    std::ifstream fichier("../Code/level6/carte1"); // Ouvre le fichier 'carte' en lecture.

    // Vérifie si le fichier a bien été ouvert.
    if (!fichier) {
        std::cerr << "Erreur lors de l'ouverture de  la carte." << std::endl;
    }

    // Lit le fichier ligne par ligne.
    std::string ligne;
    int y = 0;
    while (std::getline(fichier, ligne)) {
        // Parcourt chaque caractère de la ligne.
        for (unsigned int x = 0; x < ligne.size(); x++) {
            // Selon le caractère, crée différents éléments de la carte.
            if (ligne[x] == 'X') {
                creerObst(x, y); // Crée un obstacle.
            }
            if (ligne[x] == 'T') {
                creerTourelle(x, y); // Crée une tourelle.
            }
            if (ligne[x] == 'W') {
                EmpEnnemis.push_back(Coordonnees(x, y));
                creerEnnemi(x, y); // Crée un ennemi.
            }
            if (ligne[x] == ' ') {
                creerVide(x, y); // Crée une case vide.
            }
        }
        y++;
    }
}

// Méthode retournant la largeur de la carte.
int Niveau6::getTailleMapX() const {
    return tailleMapX;
}

// Méthode retournant la hauteur de la carte.
int Niveau6::getTailleMapY() const {
    return tailleMapY;
}

// Méthode pour créer une tourelle
void Niveau6::creerTourelle(int x, int y) {
    Tourelles.push_back(Personnage(x, y, 10, 0)); 
    EmpTours.push_back(Coordonnees(x, y));
}

// Méthode pour créer un obstacle
void Niveau6::creerObst(int x, int y) {
    Obsts.push_back(Objet(x, y, 0));
}

// Méthode pour créer un ennemi
void Niveau6::creerEnnemi(int x, int y) {
    int ptVie = 0; //2 4 6 10 
    int R = rand() % 100; 
    for (int j = 0; j < nb; j++){
        R = rand() % 100;
        if (R % 2 == 0){
            ptVie = 2;
            //std::cout << "Ennemi créé en (" << x << ", " << y << ") avec " << ptVie << " de points de vie.\n";
            break;
        }
        else if (R % 3 == 0) {
            ptVie = 6;
            //std::cout << "Ennemi créé en (" << x << ", " << y << ") avec " << ptVie << " de points de vie.\n";
            break;
        }
        else if (R % 5 == 0){
            ptVie = 10;
            //std::cout << "Ennemi créé en (" << x << ", " << y << ") avec " << ptVie << " de points de vie.\n";
            break;
        }
        else {
            ptVie = 4;
            //std::cout << "Ennemi créé en (" << x << ", " << y << ") avec " << ptVie << " de points de vie.\n";
            break;    
        }
    }
    Ennemis.push_back(Personnage(x, y, ptVie, ptVie));
    TotalEnnemis += 1;
}

// Méthode pour créer une case vide
void Niveau6::creerVide(int x, int y) {
    Vides.push_back(Objet(x, y, 0));
}

// Méthode pour déplacer un ennemi vers une cible. Calcule le plus court chemin et déplace l'ennemi.
bool Niveau6::deplacerEnnemi(int cibleX, int cibleY){
    bool ennemiDeplace; 
    //std::cout << "Déplacement vers (" << cibleX << ", " << cibleY << ")\n";
    // Parcourt tous les ennemis pour les déplacer vers la cible.
    for (auto& ennemi : Ennemis) {
        // Calcule le plus court chemin pour l'ennemi vers la cible.
        std::vector<Noeud*> chemin = calculerPlusCourtChemin(ennemi.getX(), ennemi.getY(), cibleX, cibleY);

        // Si un chemin est trouvé et qu'il contient plus d'un noeud.
        if (!chemin.empty() && chemin.size() > 1) {
            // Déplace l'ennemi vers le prochain noeud du chemin.
            Noeud* prochainPas = chemin[1];
            ennemi.setX(prochainPas->x);
            ennemi.setY(prochainPas->y);
        }
        if (ennemi.getX() == obj.getX() && ennemi.getY() == obj.getY()){
            return ennemiDeplace = false;
        }

        // Libère la mémoire allouée pour les noeuds du chemin.
        for (auto& noeud : chemin) {
            delete noeud;
        }
        if (Ennemis.empty()) {return true;}
    }
    return true;
}

// Méthode pour calculer une heuristique entre deux points, ça servira pour la méthode le plus court chemin. TOUT à la fin
int Niveau6::heuristique(int x1, int y1, int x2, int y2) {
    return std::abs(x1 - x2) + std::abs(y1 - y2);
}


// Méthode pour calculer le plus court chemin entre deux points sur la carte en utilisant un algorithme de parcours.
std::vector<Noeud*> Niveau6::calculerPlusCourtChemin(int departX, int departY, int cibleX, int cibleY) {

    // une file de priorité pour gérer les noeuds ouverts durant le parcours.
    std::priority_queue<Noeud*, std::vector<Noeud*>, CompareNoeud> voisins;
    // file prioritaire en fonction du score des noeuds, 3 param qui définie la priorité

    // Stocke les noeuds visités et leur parent.
    std::unordered_map<int, Noeud*> chemin; 
    // paires clé-valeur, comme les dictionnaires en pythons, hashage et donc des "index" pour repérer les noeuds
    
    // Crée et ajoute le noeud de départ à la frontière.
    Noeud* depart = new Noeud(departX, departY);
    voisins.push(depart);

    // Stock le noeud dans le map //
    chemin[departY * getTailleMapX() + departX] = depart;
    // la clé est défini "par convention comme telle"
    // pour une carte de largeur 10, le point (3, 2) se traduirait en 2*10 + 3 = 23.
    // le point J est converti grâce à cette clé

    // Boucle tant que la frontière n'est pas vide.
    while (!voisins.empty()) {
        // Accède au noeud  qui est en tête de la file de priorité, qui a le score total le plus bas.
        Noeud* courant = voisins.top();
        // Ciao
        voisins.pop();

        // Vérifie si le noeud courant est la cible.
        if (courant->x == cibleX && courant->y == cibleY) { // bah en fin!
            // Reconstruit le chemin inverse jusqu'au noeud de départ.
            std::vector<Noeud*> cheminInverse;
            while (courant) { // tant que courant ne point pas vers rien (nullptr)
                cheminInverse.push_back(courant);
                courant = courant->parent; // comme les listes chainées
            }

            // Inverse le chemin pour qu'il commence du départ.
            std::reverse(cheminInverse.begin(), cheminInverse.end());

            // Nettoie les noeuds non utilisés.
            for (auto& paire : chemin) {
                // pair second -> la manière de parcourir unordered map
                // La fontion *find*
                // Elle cherche paire.second (pointeur ver un Noeud)
                // Grâce à cheminInverse.begin() et cheminInverse.end() on récupère le début et la fin du vecteur cheminInverse
                // Elle recherche un élément dans le vecteur et SI elle ne le trouve pas -> elle retourne cheminInverse.end()
                if (std::find(cheminInverse.begin(), cheminInverse.end(), paire.second) == cheminInverse.end()) {
                    delete paire.second;
                }
            }
            return cheminInverse;
        }

        // Explore les noeuds voisins dans quatre directions (haut, bas, gauche, droite).
        std::vector<std::pair<int, int>> directions = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
        for (const auto& dir : directions) {
            int nx = courant->x + dir.first; // gauche et droite
            int ny = courant->y + dir.second; // haut et bas
            // le Noeud courant c'est le futur père
            // On calcule notre indice grâce à la même clé (de chemin)
            int indice = ny * getTailleMapX() + nx;

            // Vérifie si le voisin est valide et non visité, ou s'il a un meilleur coût.
            // Et qu'il soit vide surtout
            if (nx >= 0 && nx < getTailleMapX() && ny >= 0 && ny < getTailleMapY() && estVide(nx, ny)) {
                // chemin.find(indice) == chemin.end() vérifie si le noeud à l'indice (indice) n'est pas encore visité
                // On compare les couts 
                /* ex :
                cout de noeud courant est égal 2
                cout de noeud voisin est égal à 7
                (2 + 1) < 7 
                donc ça fait le coup d'y aller
                */  
                if (chemin.find(indice) == chemin.end() || courant->cout + 1 < chemin[indice]->cout) {
                    // Crée un nouveau noeud pour le voisin et l'ajoute à la frontière.
                    Noeud* voisin = new Noeud(nx, ny, courant); // courant est le père
                    voisin->cout = courant->cout + 1; // le cout de notre Noeud actuel +1
                    voisin->heuristique = heuristique(nx, ny, cibleX, cibleY); // en quelque sort c'est le coût restant pour atteindre le noeud cible.
                    chemin[indice] = voisin; // rajout à chemin
                    voisins.push(voisin); // rajout à frontière
                }
            }
        }
    }

    // Nettoie tous les noeuds si aucun chemin n'est trouvé.
    for (auto& paire : chemin) {
        delete paire.second;
    }
    return {};
}

// Méthode pour gérer l'IA des tourelles, déterminant si elles peuvent détecter et attaquer les ennemis.
void Niveau6::miniIA() {
    // Parcourt chaque tourelle pour évaluer la détection des ennemis.
    for (auto& tourelle : Tourelles) {
        for (auto& ennemi : Ennemis) {
            // Calcule la distance entre la tourelle et l'ennemi.
            int dx = std::abs(tourelle.getX() - ennemi.getX());
            int dy = std::abs(tourelle.getY() - ennemi.getY());

            // Vérifie si l'ennemi est à portée et dans la ligne de vue de la tourelle.
            //if ((dx <= 4 && dy == 0) || (dy <= 4 && dx == 0)) {
            if ((dx <= 2 && dy <= 2)) {
                // Affiche un message si l'ennemi est détecté.
                if (tourelle.getVitesse()>0) {
                    ennemi.degat(tourelle.getVitesse());
                    //std::cout << "L'tourelle en (" << tourelle.getX() << ", " << tourelle.getY() << ") a détecté un ennemi en (" << ennemi.getX() << ", " << ennemi.getY() << ")!\n" <<
                    //"cet ennemi a reçu " << tourelle.getVitesse() << " points de dégats, " << "il en a " << ennemi.getPointsDeVie() << ".\n";
                    if (!ennemi.isAlive()) {
                        jetons += (ennemi.get_vie() * -1);
                        Ennemis.pop_back();
                        compteur +=1;
                    }
                }
            }
        }
    }
}

// Méthode pour vérifier si une position donnée est vide.
bool Niveau6::estVide(int x, int y) {
    // Vérifie si la position correspond à une case vide.
    for (const auto& vide : Vides) {
        if (vide.getX() == x && vide.getY() == y) {
            return true;
        }
    }
    return false;
}

// Méthode pour vérifier si une position donnée contient un obstacle.
bool Niveau6::estObst(int x, int y) {
    // Vérifie si la position correspond à un obstacle.
    for (const auto& obst : Obsts) {
        if (obst.getX() == x && obst.getY() == y) {
            return true;
        }
    }
    return false;
}


// Méthode pour créer une tourelle personnalisée par l'utilisateur.
void Niveau6::creerTourlevel(int x, int y, int p) {
    if (jetons > 0){
        Personnage p1 = Personnage(x, y, 10, p);
        jetons -= p;

        for (unsigned int i = 0; i < Ennemis.size(); ++i) {
            if (Ennemis[i] == p1) {
                Ennemis.erase(Ennemis.begin() + i);
                break;}
        }

        Tourelles.push_back(p1);
    }
    //else //std::cout << "Vous n'avez pas assez de points !";
}

// Méthode principale pour mettre à jour le niveau. Gère le jeu et l'interaction avec l'utilisateur.
int Niveau6::mettreAJour() {
    //recupMap(); // Charge la carte du niveau.
    //creerTourlevel(); // Crée une tourelle personnalisée (actuellement en commentaire).
    //afficher(); // Affiche la carte.
    //while (true) {
        miniIA(); // Exécute l'IA des tourelles.
        // Déplace les ennemis vers une cible et vérifie si le mouvement est possible.
        if (deplacerEnnemi(obj.getX(), obj.getY()) == false) {
            //afficher();
            //std::cout << "GameOver!\n"; // Affiche GameOver si les ennemis ne peuvent pas être déplacés.
            return 2;
        };
        if (compteur == 20){
            return 1;
        }
        if (Ennemis.empty() || compteur > 20) {
            //afficher();
            //std::cout << "YouWon!\n"; // Affiche YouWon si les ennemis ne peuvent pas être déplacés.
            return 1;
        }
        int pop = (rand() % 1000);
        if (pop < 20) {
            for (auto c : EmpEnnemis){
                creerEnnemi(c.getX(), c.getY());
            }
        }
       /* //std::cout << "[0]----------------------------------------------- passer ------------------------------------------------------------> ";
        std::cin >> entree;
        
        if (entree == 't' || entree == 'T'){
            creerTourlevel();
        }
        // Sort de la boucle si 'q' est saisi.
        if (entree == 'q') {
            break;
        }*/
        //afficher(); // Réaffiche la carte après chaque tour.
    //}
    return 0; // à modifier
}

// Méthode pour afficher la carte du niveau avec les éléments du jeu.
void Niveau6::afficher() {
    // Parcourt chaque case de la carte.
    for (int i = 0; i < tailleMapY; i++) {
        for (int j = 0; j < tailleMapX; j++) {
            bool affiche = false;
            // Vérifie et affiche les tourelles.
            for (auto& tourelle : Tourelles) {
                if (tourelle.getX() == j && tourelle.getY() == i) {
                    if(tourelle.getVitesse() == 1){
                        //std::cout << "1 ";
                        affiche = true;
                        break;
                    }
                    if(tourelle.getVitesse() == 2){
                        //std::cout << "2 ";
                        affiche = true;
                        break;
                    }
                    if(tourelle.getVitesse() == 3){
                        //std::cout << "3 ";
                        affiche = true;
                        break;
                    }
                    if(tourelle.getVitesse() == 4){
                        //std::cout << "4 ";
                        affiche = true;
                        break;
                    }
                    if(tourelle.getVitesse() == 5){
                        //std::cout << "5 ";
                        affiche = true;
                        break;
                    }
                    if(tourelle.getVitesse() == 6){
                        //std::cout << "6 ";
                        affiche = true;
                        break;
                    }
                    if(tourelle.getVitesse() == 0){
                        //std::cout << "T ";
                        affiche = true;
                        break;
                    }
                }
            }
            // Vérifie et affiche les obstacles.
            for (auto& vide : Obsts) {
                if (vide.getX() == j && vide.getY() == i) {
                    //std::cout << "X ";
                    affiche = true;
                    break;
                }
            }
            // Vérifie et affiche les ennemis.
            for (auto& ennemi : Ennemis) {
                if (ennemi.getX() == j && ennemi.getY() == i) {
                    //std::cout << "E ";
                    //creerEnnemi()
                    affiche = true;
                    break;
                }
            }
            // OBJECTIF
            // if (obj.getX() == j && obj.getY() == i){
            //     //std::cout << "G";
            // }

            // Affiche un espace vide si aucune autre entité n'est présente.
            if (!affiche) {
                //std::cout << "  ";
            }
        }
        //std::cout << std::endl; // Saute une ligne à la fin de chaque rangée.
    }
    //std::cout << "TotaldeJetons : " << jetons << std::endl;
    //std::cout << "La liste des emplacements possibles des Tours : \n";
    for (auto e : EmpTours){
        e.afficher();  
    }
    // //std::cout << "La liste des emplacements possibles des Ennemis : \n";
    // for (auto e : EmpEnnemis){
    //     e.afficher();  
    // }
    //std::cout << "TotalEnnemis : " << TotalEnnemis << std::endl;
    //std::cout << "Morts : " << compteur << std::endl;
}

// ennemie avec déférent points de vie