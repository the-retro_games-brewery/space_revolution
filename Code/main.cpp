#include "main.h"
#include <memory> // Pour std::unique_ptr

int main() {
    int largeur = 1200;
    int hauteur = 840;
    float vitesse = 1;
    char cwd[1024];

    if (getcwd(cwd, sizeof(cwd)) != nullptr) {
        std::cout << "Répertoire de travail actuel : " << cwd << std::endl;
    } else {
        std::cout << "Erreur lors de la récupération du répertoire de travail actuel" << std::endl;
    }

    // Création de la fenêtre SFML
    sf::RenderWindow window(sf::VideoMode(largeur, hauteur), "SPACE_REVOLUTION");

    std::unique_ptr<SFML_level2> Niveau2 = std::make_unique<SFML_level2>(window);
    std::unique_ptr<SFML_level6> Niveau6 = std::make_unique<SFML_level6>(window);

    bool retry = true;

    sf::Event event;
    
    int menuSelection = gameMenu(window);
    int gameover = 1;
    while(menuSelection == 1){
    if (event.type == sf::Event::Closed) {
        window.close();
        return 0;
        }
        
        Niveau2->run();
        Niveau6->run();
    }
    window.close();
    return 0;
}