#include "SFML_level4.h"

SFML_level4::SFML_level4(sf::RenderWindow& window)
        : window(window), niveau(Personnage(13, 0, 10, 1)) {
    TextureLoader();
}

SFML_level4::~SFML_level4() {
}
void SFML_level4::TextureLoader() {
    if (!playerTexture.loadFromFile("../../assets/character/Player/1/eA7sqMSeSc2H07LxGAmZMQ_background-removal_1706720074 copie 4.png")) {
        std::cerr << "Erreur lors du chargement de la texture du joueur" << std::endl;
    }
    if (!carte11.loadFromFile("./../assets/Planets/ground/carte.png")) {
        std::cerr << "Erreur lors du chargement de la texture de carte11" << std::endl;
    }

    if (!enemiTexture.loadFromFile("../../assets/character/TIE Pilot.png")) {
        std::cerr << "Erreur lors du chargement de la texture de l'ennemi" << std::endl;
    }
}


void SFML_level4::Render() {
    int tailleTuileX = window.getSize().x / niveau.getTailleMapX();
    int tailleTuileY = window.getSize().y / niveau.getTailleMapY();
/*
        // Créez un objet sf::View centré sur le joueur
    sf::View view(sf::FloatRect(0, 0, window.getSize().x, window.getSize().y));
    view.setCenter(niveau.getJoueur().getX() * tailleTuileX, niveau.getJoueur().getY() * tailleTuileY);
*/

    window.clear();

    // Dessin du fond de la carte
    sf::Sprite backgroundSprite(carte11);
    backgroundSprite.setScale(window.getSize().x / backgroundSprite.getLocalBounds().width, window.getSize().y / backgroundSprite.getLocalBounds().height);
    window.draw(backgroundSprite);
        // Détermination de la taille des tuiles en fonction de la taille de la fenêtre et de la carte

    // Dessin du joueur
    // Dessin du joueur sous forme de cercle blanc
    sf::CircleShape playerCircle(0.4 * std::min(tailleTuileX, tailleTuileY)); // Création du cercle
    playerCircle.setFillColor(sf::Color::White); // Définition de la couleur
    playerCircle.setPosition(niveau.getJoueur().getX() * tailleTuileX, niveau.getJoueur().getY() * tailleTuileY); // Positionnement
    window.draw(playerCircle);

    // Dessin des balles du joueur
    for (auto& balle : niveau.getJoueur().getBalles()) {
        std::cout << "Balle : " << balle.getX() << " " << balle.getY() << std::endl;
        sf::CircleShape bulletCircle(0.2 * std::min(tailleTuileX, tailleTuileY)); // Création du cercle
        bulletCircle.setFillColor(sf::Color::White); // Définition de la couleur
        bulletCircle.setPosition(balle.getX() * tailleTuileX, balle.getY() * tailleTuileY); // Positionnement
        window.draw(bulletCircle);
    }

    // Dessin des balles ennemies

    for (auto& ennemi : niveau.getEnnemis()) {
        for (auto& balle : ennemi.getBalles()) {
            sf::CircleShape bulletCircle(0.2 * std::min(tailleTuileX, tailleTuileY)); // Création du cercle
            bulletCircle.setFillColor(sf::Color::Red); // Définition de la couleur
            bulletCircle.setPosition(balle.getX() * tailleTuileX, balle.getY() * tailleTuileY); // Positionnement
            window.draw(bulletCircle);
        }
    }

    // Dessin des ennemis
    for (auto& ennemi : niveau.getEnnemis()) {
        if (ennemi.getPointsDeVie() > 0 && ennemi.getX() > 0 && ennemi.getY() > 0) {
            sf::CircleShape enemyCircle(0.4 * std::min(tailleTuileX, tailleTuileY)); // Création du cercle
            enemyCircle.setFillColor(sf::Color::Red); // Définition de la couleur
            enemyCircle.setPosition(ennemi.getX() * tailleTuileX, ennemi.getY() * tailleTuileY); // Positionnement
            window.draw(enemyCircle);
        }
    }

    // Affichage de la barre de vie
    sf::RectangleShape healthBar(sf::Vector2f(100, 10));
    healthBar.setFillColor(sf::Color::Red);
    healthBar.setPosition(10, 10);
    window.draw(healthBar);

        // Mise à jour de la position de la vue pour suivre le joueur
    //view.setCenter(niveau.getJoueur().getX() * tailleTuileX, niveau.getJoueur().getY() * tailleTuileY);

    // Appliquez la vue
    //window.setView(view);

    window.display();
}



int SFML_level4::run() {
    sf::Clock clock; // Créez une horloge pour mesurer le temps écoulé
    float tempsEntreMisesAJour = 0.4f; // Temps entre les mises à jour en secondes
    TextureLoader();
    Render();

    niveau.recupMap();


    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
            }

            int newX = niveau.getJoueur().getX();
            int newY = niveau.getJoueur().getY();

            // Vérifiez les événements de touche pressée
            if (event.type == sf::Event::KeyPressed) {
                if (event.key.code == sf::Keyboard::Z) {
                    if (niveau.getJoueur().getY() > 0 && !niveau.estVide(newX, newY - 1)) {
                        niveau.getJoueur().setY(niveau.getJoueur().getY() - niveau.getJoueur().getVitesse());
                        niveau.getJoueur().setDirection(haut);
                    }
                } else if (event.key.code == sf::Keyboard::S) {
                    if (niveau.getJoueur().getY() < window.getSize().x - 1 && !niveau.estVide(newX, newY + 1)) {
                        niveau.getJoueur().setY(niveau.getJoueur().getY() + niveau.getJoueur().getVitesse());
                        niveau.getJoueur().setDirection(bas);
                    }
                } else if (event.key.code == sf::Keyboard::Q) {
                    if (niveau.getJoueur().getX() > 0 && !niveau.estVide(newX - 1, newY)) {
                        niveau.getJoueur().setX(niveau.getJoueur().getX() - niveau.getJoueur().getVitesse());
                        niveau.getJoueur().setDirection(gauche);
                    }
                } else if (event.key.code == sf::Keyboard::D) {
                    if (niveau.getJoueur().getX() < window.getSize().y - 1 && !niveau.estVide(newX + 1, newY)) {
                        niveau.getJoueur().setX(niveau.getJoueur().getX() + niveau.getJoueur().getVitesse());
                        niveau.getJoueur().setDirection(droite);
                    }
                } else if (event.key.code == sf::Keyboard::Escape) {
                    gamePause(window);
                }
                else if (event.key.code == sf::Keyboard::Space) {
                niveau.getJoueur().tirerBalle(niveau.getJoueur().getX(), niveau.getJoueur().getY(),1);
            }
            }
        }

            // Vérifiez si le temps écoulé dépasse le temps entre les mises à jour
            if (clock.getElapsedTime().asSeconds() >= tempsEntreMisesAJour) {
                // Réinitialisez l'horloge pour mesurer le temps écoulé jusqu'à la prochaine mise à jour
                clock.restart();

                
                int gameState = niveau.mettreAJour();

                if (gameState == 2) {
                    std::cout << "Vous avez gagné !" << std::endl;
                    return 2;
                } else if (gameState == -2) {
                    std::cout << "Vous avez perdu 1 !" << std::endl;
                    return -2;
                }

                Render();
            }

    }
    return 0;
    }

int main() {
    sf::RenderWindow window(sf::VideoMode(1200, 840), "SFML works!");
    SFML_level4 niveau(window);
    niveau.run();
    return 0;
}

//g++ -std=c++17 SFML_level4.cpp level4.cpp ../Aux/Personnage.cpp ../Aux/Objet.cpp ../Aux/Coordonnees.cpp ../Aux/Bullet.cpp ../Functions/pause.cpp -o testGLV4 -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio