#include "Level4.h"

Niveau4::Niveau4(Personnage joueur) : joueur(joueur),dernierEnnemi(std::time(nullptr)){}

Niveau4::~Niveau4() {
    viderjeuMatrix();
    viderEnnemis();
    viderBallesJoueur();
    viderBallesEnnemis();
    viderEss();
    viderObjectifs();
    viderVides();
}
void Niveau4::viderjeuMatrix() {
    jeuMatrix.clear();
}
void Niveau4::viderEnnemis() {
    ennemis.clear();
}

void Niveau4::viderBallesJoueur() {
    joueur.viderBalles();
}
void Niveau4::viderBallesEnnemis(){
    for (auto& ennemi : ennemis) {
    ennemi.viderBalles();
    }
}
void Niveau4::viderEss(){
    Ess.clear();
}
void Niveau4::viderObjectifs(){
    objectifs.clear();
}

void Niveau4::viderVides(){
    vides.clear();
}


void Niveau4::recupMap() {
    std::ifstream fichier("./carte11");
    if (!fichier) {
        std::cerr << "Erreur lors de l'ouverture de  la carte." << std::endl;

    }

    // Stocker les coordonnées des X
    std::vector <std::pair<int, int>> coords;

    std::string ligne;
    int y = 0;
    while (std::getline(fichier, ligne)) {
        for (int x = 0; x < ligne.size(); x++) {
            if (ligne[x] == 'X') {
                creerVide(x, y);
            }

            if (ligne[x] == 'E') {
                creerEnnemi(x, y);
            }

            if (ligne[x] == 'O'){
                creerObjectif(x,y);
            }
            if (ligne[x] == ' ') {
                creerEss(x, y);
            }
        }
        y++;
    }
}


int Niveau4::getTailleMapX() const {
    return tailleMapX;
}

int Niveau4::getTailleMapY() const {
    return tailleMapY;
}

const std::vector<Personnage>& Niveau4::getEnnemis() const {
    return ennemis;
}

Personnage& Niveau4::getJoueur() {
    return joueur;
}

void Niveau4::creerEnnemi(int x, int y) {
        ennemis.push_back(Personnage(x,y, 1, 1)); // Ajouter l'ennemi
}

void Niveau4::creerEss(int x, int y) {
        Ess.push_back(Objet(x,y, 1)); // Ajouter l'ennemi
}

void Niveau4::creerVide(int x, int y) {
    vides.push_back(Objet(x,y,0));
}

void Niveau4::creerObjectif(int x, int y) {
    objectifs.push_back(Objet(x,y,0));
}


void Niveau4::deplacerEnnemi() {
    for (auto& ennemi : ennemis) {
        // Générer un déplacement aléatoire
        int deplacementX = 0;
        int deplacementY = 0;

        // Déplacement horizontal
        if (rand() % 2 == 0) {
            deplacementX = (rand() % 3) - 1; // -1, 0 ou 1
        } else {
            deplacementY = (rand() % 3) - 1; // -1, 0 ou 1
        }

        // Vérifier que la nouvelle position est un espace vide avant de déplacer l'ennemi
        if (estEss(ennemi.getX() + deplacementX, ennemi.getY() + deplacementY)) {
            ennemi.setX(ennemi.getX() + deplacementX);
            ennemi.setY(ennemi.getY() + deplacementY);
        }
    }


}

void Niveau4::suivreJoueurEnnemi() {
    for (auto& ennemi : ennemis) {
        // Calcul du déplacement horizontal du joueur mais reste a 2 cases de distance
        int deplacementX = 0;
        if (ennemi.getX() < joueur.getX() && std::abs(ennemi.getX() - joueur.getX()) > 2) {
            deplacementX = 1;
        } else if (ennemi.getX() > joueur.getX() && std::abs(ennemi.getX() - joueur.getX()) > 2){
            deplacementX = -1;
        }

        // Calcul du déplacement vertical
        int deplacementY = 0;
        if (ennemi.getY() < joueur.getY() && std::abs(ennemi.getY() - joueur.getY()) > 2) {
            deplacementY = 1;
        } else if (ennemi.getY() > joueur.getY() && std::abs(ennemi.getY() - joueur.getY()) > 2){
            deplacementY = -1;
        }

        // Vérifier que la nouvelle position est un espace vide avant de déplacer l'ennemi
        if (estEss(ennemi.getX() + deplacementX, ennemi.getY() + deplacementY)) {
            ennemi.setX(ennemi.getX() + deplacementX);
            ennemi.setY(ennemi.getY() + deplacementY);
        }
    }
}

void Niveau4::miniIA() {
    for (auto& ennemi : ennemis) {
        bool detecte = false;

        // Calcul de la distance entre l'ennemi et le joueur
        int dx = std::abs(ennemi.getX() - joueur.getX());
        int dy = std::abs(ennemi.getY() - joueur.getY());

        // Si l'ennemi est à 3 cases ou moins du joueur et directement aligné soit en x soit en y
        if ((dx <= 3 && dy == 0) || (dy <= 3 && dx == 0)) {
            detecte = true; // On suppose que l'ennemi peut détecter le joueur

            // Vérification horizontale
            if (dx != 0) {
                int xMin = std::min(ennemi.getX(), joueur.getX());
                int xMax = std::max(ennemi.getX(), joueur.getX());
                for (int x = xMin + 1; x < xMax; ++x) { // Commence après xMin et s'arrête avant xMax
                    if (estVide(x, ennemi.getY())) {
                        detecte = false;
                        break; // Un vide est trouvé, l'ennemi ne peut pas détecter le joueur
                    }
                }
            }
                // Vérification verticale
            else if (dy != 0) {
                int yMin = std::min(ennemi.getY(), joueur.getY());
                int yMax = std::max(ennemi.getY(), joueur.getY());
                for (int y = yMin + 1; y < yMax; ++y) { // Commence après yMin et s'arrête avant yMax
                    if (estVide(ennemi.getX(), y)) {
                        detecte = false;
                        break; // Un vide est trouvé, l'ennemi ne peut pas détecter le joueur
                    }
                }
            }

            // Si l'ennemi détecte le joueur, il le suit
            if (detecte) {
                suivreJoueurEnnemi();
                //l'ennemi tire sur le joueur
                //  si le joueur est au dessus de l'ennemi alors l'ennemi tirre au dessus
                if(ennemi.getY() < joueur.getY()){
                    //ennemi.tirerBalle(0,1,1);
                }
                //  si le joueur est en dessous de l'ennemi alors l'ennemi tirre en dessous
                if(ennemi.getY() > joueur.getY()){
                    //ennemi.tirerBalle(0,-1,1);
                }

                //  si le joueur est à gauche de l'ennemi alors l'ennemi tirre à gauche
                if(ennemi.getX() > joueur.getX()){
                    //ennemi.tirerBalle(-1,0,1);
                }

                //  si le joueur est à droite de l'ennemi alors l'ennemi tirre à droite
                if(ennemi.getX() < joueur.getX()){
                    //ennemi.tirerBalle(1,0,1);
                }
            }
        }

        // Si le joueur n'est pas détecté, déplacer l'ennemi de manière aléatoire
        if (!detecte) {
            deplacerEnnemi();
        }
    }
}



/*g++ -std=c++17 SFML_level4.cpp level4.cpp ../Aux/Personnage.cpp ../Aux/Objet.cpp ../Aux/Coordonnees.cpp ../Aux/Bullet.cpp ../Functions/pause.cpp -o testGLV4 -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio*/


bool Niveau4::estVide(int x, int y) {
    for (const auto& vide : vides) {
        if (vide.getX() == x && vide.getY() == y) {
            return true;
        }
    }
    return false;
}

bool Niveau4::estEss(int x, int y) {
    for (const auto& vide : Ess) {
        if (vide.getX() == x && vide.getY() == y) {
            return true;
        }
    }
    return false;
}

void Niveau4::recupCoordSouris(int x, int y) {
    mouseX = x;
    mouseY = y;
    std::cout << "Coordonnées de la souris : " << mouseX << ", " << mouseY << std::endl;
}

int Niveau4::mettreAJour() {
    //recupMap();
    std::cout << "Mise à jour du niveau 4" << std::endl;

        miniIA();

    

        for(auto& objectif : objectifs){
            if(objectif.getX() == joueur.getX() && objectif.getY() == joueur.getY()){
                return 2;
            }
        }

        if(joueur.isAlive() == false){
            return -2;
        }

        if(joueur.getDirection() == gauche){
        joueur.updateBalles(-1,0);}
        if(joueur.getDirection() == droite){
        joueur.updateBalles(1,0);}
        if(joueur.getDirection() == haut){
        joueur.updateBalles(0,-1);}
        if(joueur.getDirection() == bas){
        joueur.updateBalles(0,1);}

            // Suppression des balles qui sortent de l'écran
    for (auto it = joueur.getBalles().begin(); it != joueur.getBalles().end();) {
        if (it->getX() < 0 || it->getX() >= tailleMapX || it->getY() < 0 || it->getY() >= tailleMapY) {
            joueur.supprimerBalle(*it);
            std::cout << "Balle sortie de l'écran !" << std::endl;
        } else {
            ++it;
        }
    }
    for (auto& ennemi : ennemis) {
        for (auto it = ennemi.getBalles().begin(); it != ennemi.getBalles().end();) {
            if (it->getX() < 0 || it->getX() >= tailleMapX || it->getY() < 0 || it->getY() >= tailleMapY) {
                ennemi.supprimerBalle(*it);
                std::cout << "Balle ennemie sortie de l'écran !" << std::endl;
            } else {
                ++it;
            }
        }
    }

    // gestion des collision entre les balles et l'ennemi
    for(auto& balle : joueur.getBalles()){
        for(auto& ennemi : ennemis){
            if(balle.getX() == ennemi.getX() && balle.getY() == ennemi.getY()){
                ennemi.degat(1);
            }
        }
    }

    // gestion des collision entre les balles ennemis et le joueur
    for(auto& ennemi : ennemis){
        for(auto& balle : ennemi.getBalles()){
            if(balle.getX() == joueur.getX() && balle.getY() == joueur.getY()){
                joueur.degat(1);
            }
        }
    }

    // Suppression des ennemis morts
    for (auto it = ennemis.begin(); it != ennemis.end();) {
        if (!it->isAlive()) {
            it = ennemis.erase(it);
        } else {
            ++it;
        }
    }

        
        

        //std::cout <<" commande \n";
        //std::cin >> entree;

       //afficher();
    return 0;
}
void Niveau4::afficher() {
    for (int i = 0; i < tailleMapY; i++) {
        for (int j = 0; j < tailleMapX; j++) {
            bool affiche = false;
            bool ennemiPresent = false;
            
            // Vérifier si un ennemi est présent à cette position
            for (auto& ennemi : ennemis) {
                if (ennemi.getX() == j && ennemi.getY() == i) {
                    ennemiPresent = true;
                    break;
                }
            }

            // Afficher un "E" si un ennemi est présent, sinon afficher le décor
            if (ennemiPresent) {
                std::cout << "E";
                affiche = true;
            } else {
                for (auto& vide : vides) {
                    if (vide.getX() == j && vide.getY() == i) {
                        std::cout << "X";
                        affiche = true;
                        break;
                    }
                }

                for (auto& E : Ess) {
                    if (E.getX() == j && E.getY() == i) {
                        std::cout << " ";
                        affiche = true;
                        break;
                    }
                }

                for (auto& objectif : objectifs) {
                    if (objectif.getX() == j && objectif.getY() == i) {
                        std::cout << "O";
                        affiche = true;
                        break;
                    }
                }
            }

            // Afficher un espace si aucune entité n'est présente à cette position
            if (!affiche) {
                std::cout << " ";
            }
        }
        std::cout << std::endl;
    }
}
/*
int main(){
    Personnage joueur(1,1,1,1);
    Niveau4 niveau(joueur);
    niveau.mettreAJour();
    return 0;
}
 */

// ligne pour compiler le main avec C++17
// g++ -std=c++17 -o level4 level4/Level4.cpp level4/SFML_level4.cpp level4/main.cpp