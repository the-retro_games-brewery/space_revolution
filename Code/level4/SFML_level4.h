#ifndef CODE_SFML_LEVEL4_H
#define CODE_SFML_LEVEL4_H

#include "Level4.h"

/**
 * @class SFML_level4
 * @brief Classe gérant l'affichage et le fonctionnement du quatrième niveau du jeu en utilisant la bibliothèque SFML.
 */
class SFML_level4 {
private:
    Niveau4 niveau; /**< Instance du quatrième niveau du jeu */
    sf::RenderWindow& window; /**< Référence vers la fenêtre SFML */

    // Textures des éléments du jeu
    sf::Texture playerTexture; /**< Texture du joueur */
    sf::Texture carte11; /**< Texture de la première carte */
    sf::Texture carte12; /**< Texture de la deuxième carte */
    sf::Texture carte21; /**< Texture de la troisième carte */
    sf::Texture carte22; /**< Texture de la quatrième carte */
    sf::Texture enemiTexture; /**< Texture de l'ennemi */
    sf::Texture bulletTexture; /**< Texture de la balle du joueur */
    sf::Texture ennemiBulletTexture; /**< Texture de la balle de l'ennemi */

public:
    /**
     * @brief Constructeur de la classe SFML_level4.
     * @param window La fenêtre SFML où afficher le niveau.
     */
    SFML_level4(sf::RenderWindow& window);

    /**
     * @brief Destructeur de la classe SFML_level4.
     */
    ~SFML_level4();

    /**
     * @brief Charge les textures nécessaires pour le niveau.
     */
    void TextureLoader();

    /**
     * @brief Affiche le niveau dans la fenêtre SFML.
     */
    void Render();

    /**
     * @brief Lance le quatrième niveau du jeu.
     * @return Le résultat du jeu.
     */
    int run(); // Run the game
};

#endif // CODE_SFML_LEVEL4_H
