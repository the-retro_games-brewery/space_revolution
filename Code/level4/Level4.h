#ifndef LEVEL4_H
#define LEVEL4_H

// 60 par 40

#include "../Aux/aux.h"
#include "../Aux/Objet.h"
#include "../Aux/Personnage.h"

class Niveau4 {
private:
    int tailleMapX = 60;
    int tailleMapY = 40;
    std::vector<Objet> jeuMatrix;
    std::vector<Objet> vides;
    std::vector<Personnage> ennemis;
    std::vector<Objet> objectifs;
    std::vector<Objet> Ess;
    Personnage joueur;
    char entree;


    std::time_t dernierEnnemi; // Moment de la dernière création d'ennemi
    int tempsEntreEnnemis;

public:
    Niveau4(Personnage joueur);
    ~Niveau4();
    void afficher();
    void recupMap();
    bool estVide(int x, int y);
    bool estEss(int x, int y);
    void miniIA();
    int getTailleMapX() const;
    int getTailleMapY() const;
    const std::vector<Personnage>& getEnnemis() const;
    Personnage& getJoueur();
    void creerEnnemi(int x,int y);
    void creerVide(int x,int y);
    void creerObjectif(int x, int y);
    void creerEss(int x, int y);
    int mettreAJour();
    void deplacerEnnemi();
    void suivreJoueurEnnemi();

    void viderjeuMatrix();
    void viderEnnemis();
    void viderBallesJoueur();
    void viderBallesEnnemis();
    void viderEss();
    void viderObjectifs();
    void viderVides();
    

    void recupCoordSouris(int x, int y);
    int mouseX = 0;
    int mouseY = 0;
};

#endif // LEVEL4_H
