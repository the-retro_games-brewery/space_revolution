ifeq ($(OS),Windows_NT)
    # Windows
    CC = g++ # clang++
    RM = del /Q
    EXEC = Bin\Space_Revolution.exe
    LIBS = -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio
    LDFLAGS = -L/usr/local/lib
    SRCDIR = Code\Aux Code\Functions Code\level2 # Code\level4 Code\level5 Code\level6
    INCDIR = -I.\Aux -I.\Functions -I.\level2 # -I.\level4 -I.\level5 -I.\level6
else
    # Linux
    CC = g++
    RM = rm -f
    EXEC = Bin/Space_Revolution
    LIBS = -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio
    LDFLAGS =
    SRCDIR = Code/Aux Code/Functions Code/level2 Code/level6 # Code/level4 Code/level5
    INCDIR = -I./Aux -I./Functions -I./level2 -I./level6 # -I./level4 -I./level5 
endif

CFLAGS = -std=c++17


SRCS := $(wildcard $(addsuffix /*.cpp, $(SRCDIR)))

SRCS += Code/main.cpp

OBJS = $(SRCS:.cpp=.o)


$(EXEC): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(EXEC) $(LIBS) $(LDFLAGS)


%.o: %.cpp
	$(CC) $(CFLAGS) $(INCDIR) -c $< -o $@

# Clean 
clean:
	$(RM) $(OBJS) $(EXEC) $(foreach dir,$(SRCDIR),$(wildcard $(dir)/*.o))
