# SPACE REVOLUTION 
"Space Revolution" est une véritable ode aux jeux rétro, où chaque niveau correspond à un style de gameplay emblématique de cette époque dorée du jeu vidéo.
Chaque niveau est conçu pour offrir une expérience de gameplay unique, évoquant les défis et les mécaniques caractéristiques des jeux rétro.
Ce jeu, développé dans le cadre d'un projet universitaire pour la matière Conception et Développement d'Applications, offre une expérience immersive dans un univers dystopique où la Terre a été ravagée, et où l'humanité cherche désespérément à se reconstruire dans l’espace.
les joueurs seront immergés dans une aventure palpitante à travers l'espace. Grâce à une variété de niveaux et de mécaniques de jeu, "Space Revolution" promet une expérience captivante et diversifiée.

"Space Revolution" se démarque par sa diversité de niveaux et ses mécaniques de jeu variées, offrant aux joueurs une aventure palpitante à travers différents défis et environnements. Le jeu sera développé en utilisant la bibliothèque SFML pour la partie graphique et le langage de programmation C++, garantissant ainsi des performances optimales et une expérience de jeu fluide.
Chaque niveau offre des défis uniques et stimulants, permettant aux joueurs d'explorer différentes facettes de l'univers de "Space Revolution" tout en progressant à travers l'histoire captivante du jeu.

## Participants

- Damergi Mohamed Ali (12307427)
- Kaddo Delshad (12310736)
- Alsafadi Kenan (12309550)

## Scénario

Dans un lointain futur, l'humanité a sombré dans une ère de décadence morale et spirituelle, poussée par l'avidité et l'égocentrisme. Alors que la Terre se languissait sous le poids de ses maux auto-infligés, une secte mystérieuse émergea des entrailles de la société. Se détachant de l'humanité corrompue, ces adeptes se retirèrent dans l'isolement, cherchant refuge sur une planète lointaine où ils espéraient cultiver leur vision d'une existence pure et transcendante.

Pendant un siècle, ils vécurent dans l'ombre, se préparant dans le secret à une mission sacrée : retourner sur la Terre profanée et purifier la planète de toute trace de corruption. Convaincus que leur voie était la seule véritablement juste, les membres de la secte justifièrent leur retour comme un acte de jugement divin, une punition infligée à une humanité déchue.

Pendant ce temps, les descendants des exilés terrestres à bord de l'Arche se sont établis sur une nouvelle planète, mais l'empreinte de leur ancien monde les hante. Tandis qu'ils s'efforcent de construire une société meilleure, ils découvrent que la menace de la secte plane toujours sur eux, rappelant les péchés de leurs ancêtres. Eva, une guerrière intrépide, se retrouve à la tête d'une mission visant à sécuriser leur nouveau foyer en éliminant la menace sectaire une fois pour toutes. Avec une détermination inébranlable, elle guide son équipe à travers les étoiles, déterminée à protéger leur monde des forces sombres qui cherchent à le corrompre.

Dans ce voyage épique à travers les confins de l'espace, Eva et ses compagnons affronteront non seulement les dangers physiques de l'univers, mais aussi les dilemmes moraux qui les confronteront à la véritable nature de l'humanité. Alors que les étoiles deviennent le théâtre de leur destinée, ils devront trouver la force de se battre pour un avenir où la lumière triomphe sur les ténèbres, où la rédemption est possible même dans les recoins les plus sombres de l'univers.

## Niveaux
### Niveau 2
Les cieux s'embrasent alors que vous entrez dans l'atmosphère de la planète, où les batailles aériennes font rage. Prenez les commandes de votre vaisseau et affrontez les forces ennemies dans un duel palpitant au-dessus des paysages époustouflants de la planète. Esquivez les tirs ennemis, manœuvrez habilement et montrez votre bravoure dans les cieux tumultueux de ce monde exotique.

### Niveau 3 (Niveau encore en cours de developpement)
Embrassez l'essence de l'excitation rétro avec une course endiablée à travers les paysages extraterrestres. Chevauchez votre moto de l'espace à travers des canyons déchiquetés et des étendues désertiques, défiant la gravité et la vitesse. Inspiré par l'intensité d’Outrun.

### Niveau 4

Plongez dans une mission d'infiltration captivante en incarnant Eva l’intrépide astronaute infiltrée dans la base ennemie sur la planète. Avec une vue du dessus. Évitez les patrouilles ennemies Et frayer vous un chemin vers le bâtiment principale afin de capturer la base.
Seul le mélange parfait de furtivité et de stratégie mènera à la victoire.

### Niveau 5

Prouver vos compétences de hacking devant ce terminal Unix, dans cette mission de hacking où vous allez devoir naviguer à travers un terminal Unix complexe pour accéder aux défenses de la base ennemie.

### Niveau 6
Prenez position et défendez votre base contre les vagues d'ennemis qui approchent. Construisez des tours défensives, améliorez vos armes et déploiez des tactiques stratégiques pour repousser l'assaut ennemi. Restez vigilant car les attaques deviennent de plus en plus intenses et seuls les plus habiles stratèges survivront à cette épreuve de défense désespérée.


## Choix graphiques
Les graphismes de "Space Revolution" sont un hommage vibrant aux jeux rétro, réalisés dans un style pixel art qui évoque avec nostalgie les époques passées du jeu vidéo.
Les graphismes évoluent pour refléter les différentes atmosphères et environnements à travers lesquels les joueurs voyagent. Dans les niveaux spatiaux, les écrans sont illuminés par des étoiles scintillantes et des nébuleuses chatoyantes, créant une toile de fond époustouflante pour les batailles spatiales et les voyages intergalactiques.

Lorsque les joueurs descendent sur la planète rouge désolée, les graphismes capturent l'essence même de cet environnement hostile. Les vastes étendues de sable rougeâtre s'étendent à perte de vue, ponctuées par des formations rocheuses ciselées par les éléments. Le ciel au-dessus est d'un rouge profond, teinté par la poussière et les tempêtes de sable occasionnelles.

Le style pixel art de "Space Revolution" crée une esthétique intemporelle qui résonne avec les joueurs de toutes les générations, rappelant les jours où l'imagination était la clé de l'expérience de jeu. Chaque élément visuel est conçu avec soin pour évoquer un sentiment de familiarité tout en offrant une expérience visuelle rafraîchissante et captivante.


## Arborescence du Projet

Le projet est structuré comme suit :

- Code
  - level1
  - level2
  - level3
  - level4
  - level5
  - level6
- Assets
- Bin
- Logo
- Sounds
## Installation et Exécution
### Dependances
- C++ 11 ou plus
- SFML 2

### Execution
une fois le depot cloné vous navez qu'a executer le makefile a l'aide de la commande [make] puis executez le programme qui se trouve dans le repertoire [Bin]
